import serial
import subprocess
import sys
import os
import errno
import yaml
from assistants.QboTalk import QBOtalk
from controller.QboController import Controller
import time

import paho.mqtt.client as mqtt
import json

port = '/dev/serial0'
config = yaml.safe_load(open("/opt/qbo/config.yml"))

MQTT_Server = "87.214.54.242"
MQTT_port = 1883

def WaitForTouch():

	touch_str = ""
	touch = HeadServo.GetHeadCmd("GET_TOUCH", 0)

	if touch:

		if touch == [1]:
			touch_str = "Touch: right"
		elif touch == [2]:
			touch_str = "Touch: up"
		elif touch == [3]:
			touch_str = "Touch: left"

		if touch == [1] or touch == [2] or touch == [3]:
			print("Touched: " + touch_str )		

	time.sleep(.250)
	return touch_str



try:

	# Open serial port
	ser = serial.Serial(port, baudrate=115200, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE, rtscts=False, dsrdtr=False, timeout=0)
	print "Open serial port sucessfully."
	print(ser.name)

	HeadServo = Controller(ser)

except:
	print "Error opening serial port."
	sys.exit()

while True:
	time.sleep(1)
	WaitForTouch()
