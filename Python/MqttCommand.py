import serial
import subprocess
import sys
import os
import errno
import yaml
from assistants.QboTalk import QBOtalk
from controller.QboController import Controller

import paho.mqtt.client as mqtt
import json
import logging
import time

port = '/dev/serial0'
config = yaml.safe_load(open("/opt/qbo/config.yml"))

MQTT_Server = "87.214.54.242"
MQTT_port = 1883

# The callback for when the client receives a CONNACK response from the server.
def on_connect(client, userdata, flags, rc):
    print("Connected with result code "+str(rc))
    logging.info('Connected to mqtt broker')

    # Subscribing in on_connect() means that if we lose the connection and
    # reconnect then subscriptions will be renewed.
    client.subscribe("qbo/command")
    HeadServo.SetNoseColor(4)

# The callback for when a PUBLISH message is received from the server.
def on_message(client, userdata, msg):
    jsonStr = str(msg.payload)
    print(msg.topic+" "+jsonStr)

    if msg.topic == "qbo/command" :
        parse_json(jsonStr)

def parse_json(jsonString):
    global HeadServo   
    #print("Command: "+dat["Command"])
    dat = json.loads(jsonString)
    cmd = dat["Command"]
    if cmd == "Mouth":
        expression = dat["Expression"]
        if expression == "smile":
            HeadServo.SetMouth(0x110E00)
        elif expression == "sad":
            HeadServo.SetMouth(0x0E1100)
        elif expression == "serious":
            HeadServo.SetMouth(0x1F1F00)
        elif expression == "love":
            HeadServo.SetMouth(0x1B1F0E04)
        elif expression == "surprise":
            HeadServo.SetMouth(0x0E11110E)
        elif expression == "none":
            HeadServo.SetMouth(0x000000)  
        elif expression == "angry":
            HeadServo.SetMouth(0x040A1100)  
        elif expression == "disgust":
            HeadServo.SetMouth(0x000A1500)
        elif expression == "fear":
            HeadServo.SetMouth(0x00000400)

        print("Mouth " + expression)
    elif cmd == "Nose":
        color = dat["Color"]
        print("Color " + color)
        if color == "none":
            HeadServo.SetNoseColor(0)
        if color == "red":
	        HeadServo.SetNoseColor(2)
        if color == "blue":
            HeadServo.SetNoseColor(1)
        if color == "green":
            HeadServo.SetNoseColor(4)
    elif cmd == "Servo":
        axis = dat["Axis"]
        angle = dat["Angle"]
        speed = dat["Speed"]
        print("Servo - Axis: " + str(axis) + ", Angle: " + str(angle) + ", Speed: " + str(speed))
        if angle!=0 and axis != 0 and speed!=0:
            HeadServo.SetServo(axis, angle, speed)
    elif cmd == "MoveRel":
        axis = dat["Axis"]
        angle = dat["Angle"]
        print("Move relative - Axis: " + str(axis) + ", Angle: " + str(angle))
        if angle!=0 and axis != 0:
            HeadServo.SetAngleRelative(axis, angle)

    else:
        print("Unknown command " + cmd)


logging.basicConfig(filename='/opt/qbo/logs/pythonMqtt.log', filemode='w', format='%(name)s - %(levelname)s - %(message)s')

try:
    # Open serial port
    ser = serial.Serial(port, baudrate=115200, bytesize=serial.EIGHTBITS, stopbits=serial.STOPBITS_ONE, parity=serial.PARITY_NONE, rtscts=False, dsrdtr=False, timeout=0)
    print "Open serial port sucessfully."
    print(ser.name)
    logging.info('Connected to serial port')
    HeadServo = Controller(ser)
    HeadServo.SetNoseColor(0)
    time.sleep(1)
    HeadServo.SetNoseColor(3)
    time.sleep(1)
    HeadServo.SetNoseColor(0)
    time.sleep(1)
    HeadServo.SetNoseColor(3)
    time.sleep(1)
    HeadServo.SetNoseColor(0)
    time.sleep(1)
    HeadServo.SetNoseColor(3)

except:
    print "Error opening serial port."
    logging.error('Error opening serial port')
    sys.exit()

try:
    client = mqtt.Client()
    client.on_connect = on_connect
    client.on_message = on_message

    client.connect(MQTT_Server, MQTT_port, 60)



# Blocking call that processes network traffic, dispatches callbacks and
# handles reconnecting.
# Other loop*() functions are available that give a threaded interface and a
# manual interface.
    client.loop_forever()
except:
    print "Could not connect to mqtt"
