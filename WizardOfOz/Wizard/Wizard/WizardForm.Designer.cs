﻿
namespace Wizard
{
    partial class WizardForm
    {
        /// <summary>
        ///  Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        ///  Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        ///  Required method for Designer support - do not modify
        ///  the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabPage2 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.listBoxQuestions = new System.Windows.Forms.ListBox();
            this.listBoxAudio = new System.Windows.Forms.ListBox();
            this.txtBoxHtml = new System.Windows.Forms.RichTextBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.btnHtml = new System.Windows.Forms.Button();
            this.txtSpeech = new System.Windows.Forms.RichTextBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioFemale = new System.Windows.Forms.RadioButton();
            this.radioMale = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioExternal = new System.Windows.Forms.RadioButton();
            this.radioInternal = new System.Windows.Forms.RadioButton();
            this.btnSpeak = new System.Windows.Forms.Button();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.btnMoveRandom = new System.Windows.Forms.Button();
            this.btnResetVert = new System.Windows.Forms.Button();
            this.txtVert = new System.Windows.Forms.TextBox();
            this.txtHor = new System.Windows.Forms.TextBox();
            this.btnResetAngleHor = new System.Windows.Forms.Button();
            this.btnMoveVert = new System.Windows.Forms.Button();
            this.btnMoveHor = new System.Windows.Forms.Button();
            this.trackBarVert = new System.Windows.Forms.TrackBar();
            this.trackBarHor = new System.Windows.Forms.TrackBar();
            this.panel4 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.listViewNose = new System.Windows.Forms.ListView();
            this.columnHeader1 = new System.Windows.Forms.ColumnHeader();
            this.listViewMouth = new System.Windows.Forms.ListView();
            this.columnHeader2 = new System.Windows.Forms.ColumnHeader();
            this.tabPage3 = new System.Windows.Forms.TabPage();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBoxSnapShot = new System.Windows.Forms.PictureBox();
            this.panel5 = new System.Windows.Forms.Panel();
            this.lblVidStatus = new System.Windows.Forms.Label();
            this.btnTestImage = new System.Windows.Forms.Button();
            this.btnStopVid = new System.Windows.Forms.Button();
            this.btnStartVid = new System.Windows.Forms.Button();
            this.panel6 = new System.Windows.Forms.Panel();
            this.lblAudioStatus = new System.Windows.Forms.Label();
            this.btnAudioStop = new System.Windows.Forms.Button();
            this.btnAudioStart = new System.Windows.Forms.Button();
            this.tabPage4 = new System.Windows.Forms.TabPage();
            this.panel7 = new System.Windows.Forms.Panel();
            this.bttnSurvey = new System.Windows.Forms.Button();
            this.bttnRestart = new System.Windows.Forms.Button();
            this.btnRestart = new System.Windows.Forms.Button();
            this.btnCloseBrowser = new System.Windows.Forms.Button();
            this.btnShutdownVid = new System.Windows.Forms.Button();
            this.statusStrip1 = new System.Windows.Forms.StatusStrip();
            this.toolStripStatusLabel1 = new System.Windows.Forms.ToolStripStatusLabel();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.tabControl1.SuspendLayout();
            this.tabPage2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVert)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHor)).BeginInit();
            this.panel4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tabPage3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSnapShot)).BeginInit();
            this.panel5.SuspendLayout();
            this.panel6.SuspendLayout();
            this.tabPage4.SuspendLayout();
            this.panel7.SuspendLayout();
            this.statusStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabPage2);
            this.tabControl1.Controls.Add(this.tabPage1);
            this.tabControl1.Controls.Add(this.tabPage3);
            this.tabControl1.Controls.Add(this.tabPage4);
            this.tabControl1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tabControl1.Location = new System.Drawing.Point(0, 0);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1256, 590);
            this.tabControl1.TabIndex = 0;
            // 
            // tabPage2
            // 
            this.tabPage2.Controls.Add(this.tableLayoutPanel2);
            this.tabPage2.Location = new System.Drawing.Point(4, 24);
            this.tabPage2.Name = "tabPage2";
            this.tabPage2.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage2.Size = new System.Drawing.Size(1248, 562);
            this.tabPage2.TabIndex = 1;
            this.tabPage2.Text = "Talking";
            this.tabPage2.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.44784F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.55216F));
            this.tableLayoutPanel2.Controls.Add(this.listBoxQuestions, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.listBoxAudio, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtBoxHtml, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtSpeech, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.panel2, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 36.33094F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 63.66906F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 155F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1242, 556);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // listBoxQuestions
            // 
            this.listBoxQuestions.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxQuestions.FormattingEnabled = true;
            this.listBoxQuestions.ItemHeight = 15;
            this.listBoxQuestions.Location = new System.Drawing.Point(3, 148);
            this.listBoxQuestions.Name = "listBoxQuestions";
            this.listBoxQuestions.Size = new System.Drawing.Size(844, 249);
            this.listBoxQuestions.TabIndex = 2;
            this.listBoxQuestions.SelectedIndexChanged += new System.EventHandler(this.listBoxQuestions_SelectedIndexChanged);
            // 
            // listBoxAudio
            // 
            this.listBoxAudio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listBoxAudio.FormattingEnabled = true;
            this.listBoxAudio.ItemHeight = 15;
            this.listBoxAudio.Location = new System.Drawing.Point(853, 148);
            this.listBoxAudio.Name = "listBoxAudio";
            this.listBoxAudio.Size = new System.Drawing.Size(386, 249);
            this.listBoxAudio.TabIndex = 3;
            this.listBoxAudio.SelectedIndexChanged += new System.EventHandler(this.listBoxAudio_SelectedIndexChanged);
            // 
            // txtBoxHtml
            // 
            this.txtBoxHtml.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtBoxHtml.Location = new System.Drawing.Point(3, 3);
            this.txtBoxHtml.Name = "txtBoxHtml";
            this.txtBoxHtml.Size = new System.Drawing.Size(844, 139);
            this.txtBoxHtml.TabIndex = 0;
            this.txtBoxHtml.Text = "";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.btnHtml);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(853, 3);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(386, 139);
            this.panel1.TabIndex = 1;
            // 
            // btnHtml
            // 
            this.btnHtml.Location = new System.Drawing.Point(21, 17);
            this.btnHtml.Name = "btnHtml";
            this.btnHtml.Size = new System.Drawing.Size(133, 23);
            this.btnHtml.TabIndex = 0;
            this.btnHtml.Text = "Send HTML";
            this.btnHtml.UseVisualStyleBackColor = true;
            this.btnHtml.Click += new System.EventHandler(this.btnHtml_Click);
            // 
            // txtSpeech
            // 
            this.txtSpeech.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSpeech.Location = new System.Drawing.Point(3, 403);
            this.txtSpeech.Name = "txtSpeech";
            this.txtSpeech.Size = new System.Drawing.Size(844, 150);
            this.txtSpeech.TabIndex = 4;
            this.txtSpeech.Text = "";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.groupBox2);
            this.panel2.Controls.Add(this.groupBox1);
            this.panel2.Controls.Add(this.btnSpeak);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(853, 403);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(386, 150);
            this.panel2.TabIndex = 5;
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioFemale);
            this.groupBox2.Controls.Add(this.radioMale);
            this.groupBox2.Location = new System.Drawing.Point(183, 13);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(135, 72);
            this.groupBox2.TabIndex = 4;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Voice";
            // 
            // radioFemale
            // 
            this.radioFemale.AutoSize = true;
            this.radioFemale.Checked = true;
            this.radioFemale.Location = new System.Drawing.Point(17, 42);
            this.radioFemale.Name = "radioFemale";
            this.radioFemale.Size = new System.Drawing.Size(63, 19);
            this.radioFemale.TabIndex = 1;
            this.radioFemale.TabStop = true;
            this.radioFemale.Text = "Female";
            this.radioFemale.UseVisualStyleBackColor = true;
            // 
            // radioMale
            // 
            this.radioMale.AutoSize = true;
            this.radioMale.Location = new System.Drawing.Point(17, 17);
            this.radioMale.Name = "radioMale";
            this.radioMale.Size = new System.Drawing.Size(51, 19);
            this.radioMale.TabIndex = 0;
            this.radioMale.Text = "Male";
            this.radioMale.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioExternal);
            this.groupBox1.Controls.Add(this.radioInternal);
            this.groupBox1.Location = new System.Drawing.Point(21, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(122, 72);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Speaker";
            // 
            // radioExternal
            // 
            this.radioExternal.AutoSize = true;
            this.radioExternal.Checked = true;
            this.radioExternal.Location = new System.Drawing.Point(16, 42);
            this.radioExternal.Name = "radioExternal";
            this.radioExternal.Size = new System.Drawing.Size(67, 19);
            this.radioExternal.TabIndex = 1;
            this.radioExternal.TabStop = true;
            this.radioExternal.Text = "External";
            this.radioExternal.UseVisualStyleBackColor = true;
            // 
            // radioInternal
            // 
            this.radioInternal.AutoSize = true;
            this.radioInternal.Location = new System.Drawing.Point(16, 17);
            this.radioInternal.Name = "radioInternal";
            this.radioInternal.Size = new System.Drawing.Size(65, 19);
            this.radioInternal.TabIndex = 2;
            this.radioInternal.Text = "Internal";
            this.radioInternal.UseVisualStyleBackColor = true;
            // 
            // btnSpeak
            // 
            this.btnSpeak.Location = new System.Drawing.Point(21, 103);
            this.btnSpeak.Name = "btnSpeak";
            this.btnSpeak.Size = new System.Drawing.Size(122, 23);
            this.btnSpeak.TabIndex = 0;
            this.btnSpeak.Text = "Talk";
            this.btnSpeak.UseVisualStyleBackColor = true;
            this.btnSpeak.Click += new System.EventHandler(this.btnSpeak_Click);
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.tableLayoutPanel1);
            this.tabPage1.Location = new System.Drawing.Point(4, 24);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage1.Size = new System.Drawing.Size(1248, 562);
            this.tabPage1.TabIndex = 0;
            this.tabPage1.Text = "QBO Head";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.30534F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.69466F));
            this.tableLayoutPanel1.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel4, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 74.51923F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25.48077F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1242, 556);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.btnMoveRandom);
            this.panel3.Controls.Add(this.btnResetVert);
            this.panel3.Controls.Add(this.txtVert);
            this.panel3.Controls.Add(this.txtHor);
            this.panel3.Controls.Add(this.btnResetAngleHor);
            this.panel3.Controls.Add(this.btnMoveVert);
            this.panel3.Controls.Add(this.btnMoveHor);
            this.panel3.Controls.Add(this.trackBarVert);
            this.panel3.Controls.Add(this.trackBarHor);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(742, 408);
            this.panel3.TabIndex = 0;
            // 
            // btnMoveRandom
            // 
            this.btnMoveRandom.Location = new System.Drawing.Point(463, 377);
            this.btnMoveRandom.Name = "btnMoveRandom";
            this.btnMoveRandom.Size = new System.Drawing.Size(228, 23);
            this.btnMoveRandom.TabIndex = 8;
            this.btnMoveRandom.Text = "Move Head Randomly";
            this.btnMoveRandom.UseVisualStyleBackColor = true;
            this.btnMoveRandom.Click += new System.EventHandler(this.btnMoveRandom_Click);
            // 
            // btnResetVert
            // 
            this.btnResetVert.Location = new System.Drawing.Point(205, 377);
            this.btnResetVert.Name = "btnResetVert";
            this.btnResetVert.Size = new System.Drawing.Size(75, 23);
            this.btnResetVert.TabIndex = 7;
            this.btnResetVert.Text = "Reset";
            this.btnResetVert.UseVisualStyleBackColor = true;
            this.btnResetVert.Click += new System.EventHandler(this.btnResetVert_Click);
            // 
            // txtVert
            // 
            this.txtVert.Location = new System.Drawing.Point(102, 225);
            this.txtVert.Name = "txtVert";
            this.txtVert.Size = new System.Drawing.Size(67, 23);
            this.txtVert.TabIndex = 6;
            // 
            // txtHor
            // 
            this.txtHor.Location = new System.Drawing.Point(325, 86);
            this.txtHor.Name = "txtHor";
            this.txtHor.Size = new System.Drawing.Size(51, 23);
            this.txtHor.TabIndex = 5;
            this.txtHor.Text = "511";
            // 
            // btnResetAngleHor
            // 
            this.btnResetAngleHor.Location = new System.Drawing.Point(533, 133);
            this.btnResetAngleHor.Name = "btnResetAngleHor";
            this.btnResetAngleHor.Size = new System.Drawing.Size(88, 23);
            this.btnResetAngleHor.TabIndex = 4;
            this.btnResetAngleHor.Text = "Reset";
            this.btnResetAngleHor.UseVisualStyleBackColor = true;
            this.btnResetAngleHor.Click += new System.EventHandler(this.btnResetAngles_Click);
            // 
            // btnMoveVert
            // 
            this.btnMoveVert.Location = new System.Drawing.Point(38, 377);
            this.btnMoveVert.Name = "btnMoveVert";
            this.btnMoveVert.Size = new System.Drawing.Size(161, 23);
            this.btnMoveVert.TabIndex = 3;
            this.btnMoveVert.Text = "Move Vertically";
            this.btnMoveVert.UseVisualStyleBackColor = true;
            this.btnMoveVert.Click += new System.EventHandler(this.btnMoveVert_Click);
            // 
            // btnMoveHor
            // 
            this.btnMoveHor.Location = new System.Drawing.Point(533, 97);
            this.btnMoveHor.Name = "btnMoveHor";
            this.btnMoveHor.Size = new System.Drawing.Size(158, 23);
            this.btnMoveHor.TabIndex = 2;
            this.btnMoveHor.Text = "Move Horizontally";
            this.btnMoveHor.UseVisualStyleBackColor = true;
            this.btnMoveHor.Click += new System.EventHandler(this.btnMoveHor_Click);
            // 
            // trackBarVert
            // 
            this.trackBarVert.Location = new System.Drawing.Point(38, 133);
            this.trackBarVert.Maximum = 32;
            this.trackBarVert.Name = "trackBarVert";
            this.trackBarVert.Orientation = System.Windows.Forms.Orientation.Vertical;
            this.trackBarVert.Size = new System.Drawing.Size(45, 224);
            this.trackBarVert.TabIndex = 1;
            this.trackBarVert.Value = 16;
            this.trackBarVert.Scroll += new System.EventHandler(this.trackBar2_Scroll);
            // 
            // trackBarHor
            // 
            this.trackBarHor.Location = new System.Drawing.Point(38, 35);
            this.trackBarHor.Maximum = 100;
            this.trackBarHor.Name = "trackBarHor";
            this.trackBarHor.Size = new System.Drawing.Size(618, 45);
            this.trackBarHor.TabIndex = 0;
            this.trackBarHor.Value = 51;
            this.trackBarHor.Scroll += new System.EventHandler(this.trackBar1_Scroll);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.tableLayoutPanel4);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(751, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(488, 408);
            this.panel4.TabIndex = 1;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Controls.Add(this.listViewNose, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.listViewMouth, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(488, 408);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // listViewNose
            // 
            this.listViewNose.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader1});
            this.listViewNose.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewNose.HideSelection = false;
            this.listViewNose.Location = new System.Drawing.Point(247, 3);
            this.listViewNose.Name = "listViewNose";
            this.listViewNose.Size = new System.Drawing.Size(238, 402);
            this.listViewNose.TabIndex = 1;
            this.listViewNose.UseCompatibleStateImageBehavior = false;
            this.listViewNose.View = System.Windows.Forms.View.Details;
            this.listViewNose.SelectedIndexChanged += new System.EventHandler(this.listViewNose_SelectedIndexChanged);
            // 
            // columnHeader1
            // 
            this.columnHeader1.Text = "Nose Color";
            this.columnHeader1.Width = 120;
            // 
            // listViewMouth
            // 
            this.listViewMouth.Columns.AddRange(new System.Windows.Forms.ColumnHeader[] {
            this.columnHeader2});
            this.listViewMouth.Dock = System.Windows.Forms.DockStyle.Fill;
            this.listViewMouth.HideSelection = false;
            this.listViewMouth.Location = new System.Drawing.Point(3, 3);
            this.listViewMouth.Name = "listViewMouth";
            this.listViewMouth.Size = new System.Drawing.Size(238, 402);
            this.listViewMouth.TabIndex = 2;
            this.listViewMouth.UseCompatibleStateImageBehavior = false;
            this.listViewMouth.View = System.Windows.Forms.View.Details;
            this.listViewMouth.SelectedIndexChanged += new System.EventHandler(this.listViewMouth_SelectedIndexChanged);
            // 
            // columnHeader2
            // 
            this.columnHeader2.Text = "Facial Expression";
            this.columnHeader2.Width = 120;
            // 
            // tabPage3
            // 
            this.tabPage3.Controls.Add(this.tableLayoutPanel3);
            this.tabPage3.Location = new System.Drawing.Point(4, 24);
            this.tabPage3.Name = "tabPage3";
            this.tabPage3.Size = new System.Drawing.Size(1248, 562);
            this.tabPage3.TabIndex = 2;
            this.tabPage3.Text = "Recording";
            this.tabPage3.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.01603F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.98397F));
            this.tableLayoutPanel3.Controls.Add(this.pictureBoxSnapShot, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel5, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel6, 1, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 67.61566F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 32.38434F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1248, 562);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // pictureBoxSnapShot
            // 
            this.pictureBoxSnapShot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBoxSnapShot.Location = new System.Drawing.Point(3, 3);
            this.pictureBoxSnapShot.Name = "pictureBoxSnapShot";
            this.pictureBoxSnapShot.Size = new System.Drawing.Size(743, 374);
            this.pictureBoxSnapShot.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.pictureBoxSnapShot.TabIndex = 0;
            this.pictureBoxSnapShot.TabStop = false;
            // 
            // panel5
            // 
            this.panel5.Controls.Add(this.lblVidStatus);
            this.panel5.Controls.Add(this.btnTestImage);
            this.panel5.Controls.Add(this.btnStopVid);
            this.panel5.Controls.Add(this.btnStartVid);
            this.panel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel5.Location = new System.Drawing.Point(752, 3);
            this.panel5.Name = "panel5";
            this.panel5.Size = new System.Drawing.Size(493, 374);
            this.panel5.TabIndex = 1;
            // 
            // lblVidStatus
            // 
            this.lblVidStatus.AutoSize = true;
            this.lblVidStatus.Location = new System.Drawing.Point(27, 187);
            this.lblVidStatus.Name = "lblVidStatus";
            this.lblVidStatus.Size = new System.Drawing.Size(45, 15);
            this.lblVidStatus.TabIndex = 3;
            this.lblVidStatus.Text = "Status: ";
            // 
            // btnTestImage
            // 
            this.btnTestImage.Location = new System.Drawing.Point(27, 90);
            this.btnTestImage.Name = "btnTestImage";
            this.btnTestImage.Size = new System.Drawing.Size(124, 23);
            this.btnTestImage.TabIndex = 2;
            this.btnTestImage.Text = "Get Test Image";
            this.btnTestImage.UseVisualStyleBackColor = true;
            this.btnTestImage.Click += new System.EventHandler(this.btnTestImage_Click);
            // 
            // btnStopVid
            // 
            this.btnStopVid.Location = new System.Drawing.Point(228, 27);
            this.btnStopVid.Name = "btnStopVid";
            this.btnStopVid.Size = new System.Drawing.Size(137, 23);
            this.btnStopVid.TabIndex = 1;
            this.btnStopVid.Text = "Stop Video Recording";
            this.btnStopVid.UseVisualStyleBackColor = true;
            this.btnStopVid.Click += new System.EventHandler(this.btnStopVid_Click);
            // 
            // btnStartVid
            // 
            this.btnStartVid.Location = new System.Drawing.Point(27, 27);
            this.btnStartVid.Name = "btnStartVid";
            this.btnStartVid.Size = new System.Drawing.Size(156, 23);
            this.btnStartVid.TabIndex = 0;
            this.btnStartVid.Text = "Start Video Recording";
            this.btnStartVid.UseVisualStyleBackColor = true;
            this.btnStartVid.Click += new System.EventHandler(this.btnStartVid_Click);
            // 
            // panel6
            // 
            this.panel6.Controls.Add(this.lblAudioStatus);
            this.panel6.Controls.Add(this.btnAudioStop);
            this.panel6.Controls.Add(this.btnAudioStart);
            this.panel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel6.Location = new System.Drawing.Point(752, 383);
            this.panel6.Name = "panel6";
            this.panel6.Size = new System.Drawing.Size(493, 176);
            this.panel6.TabIndex = 2;
            // 
            // lblAudioStatus
            // 
            this.lblAudioStatus.AutoSize = true;
            this.lblAudioStatus.Location = new System.Drawing.Point(38, 119);
            this.lblAudioStatus.Name = "lblAudioStatus";
            this.lblAudioStatus.Size = new System.Drawing.Size(42, 15);
            this.lblAudioStatus.TabIndex = 3;
            this.lblAudioStatus.Text = "Status:";
            // 
            // btnAudioStop
            // 
            this.btnAudioStop.Location = new System.Drawing.Point(210, 57);
            this.btnAudioStop.Name = "btnAudioStop";
            this.btnAudioStop.Size = new System.Drawing.Size(167, 23);
            this.btnAudioStop.TabIndex = 2;
            this.btnAudioStop.Text = "Stop Audio Recording";
            this.btnAudioStop.UseVisualStyleBackColor = true;
            this.btnAudioStop.Click += new System.EventHandler(this.btnAudioStop_Click);
            // 
            // btnAudioStart
            // 
            this.btnAudioStart.Location = new System.Drawing.Point(38, 57);
            this.btnAudioStart.Name = "btnAudioStart";
            this.btnAudioStart.Size = new System.Drawing.Size(145, 23);
            this.btnAudioStart.TabIndex = 0;
            this.btnAudioStart.Text = "Start Audio Recording";
            this.btnAudioStart.UseVisualStyleBackColor = true;
            this.btnAudioStart.Click += new System.EventHandler(this.btnAudioStart_Click);
            // 
            // tabPage4
            // 
            this.tabPage4.Controls.Add(this.panel7);
            this.tabPage4.Location = new System.Drawing.Point(4, 24);
            this.tabPage4.Name = "tabPage4";
            this.tabPage4.Padding = new System.Windows.Forms.Padding(3);
            this.tabPage4.Size = new System.Drawing.Size(1248, 562);
            this.tabPage4.TabIndex = 3;
            this.tabPage4.Text = "Shutdown";
            this.tabPage4.UseVisualStyleBackColor = true;
            // 
            // panel7
            // 
            this.panel7.Controls.Add(this.bttnSurvey);
            this.panel7.Controls.Add(this.bttnRestart);
            this.panel7.Controls.Add(this.btnRestart);
            this.panel7.Controls.Add(this.btnCloseBrowser);
            this.panel7.Controls.Add(this.btnShutdownVid);
            this.panel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel7.Location = new System.Drawing.Point(3, 3);
            this.panel7.Name = "panel7";
            this.panel7.Size = new System.Drawing.Size(1242, 556);
            this.panel7.TabIndex = 0;
            // 
            // bttnSurvey
            // 
            this.bttnSurvey.Location = new System.Drawing.Point(440, 161);
            this.bttnSurvey.Name = "bttnSurvey";
            this.bttnSurvey.Size = new System.Drawing.Size(146, 23);
            this.bttnSurvey.TabIndex = 4;
            this.bttnSurvey.Text = "Restart Survey";
            this.bttnSurvey.UseVisualStyleBackColor = true;
            this.bttnSurvey.Click += new System.EventHandler(this.bttnSurvey_Click);
            // 
            // bttnRestart
            // 
            this.bttnRestart.Location = new System.Drawing.Point(30, 517);
            this.bttnRestart.Name = "bttnRestart";
            this.bttnRestart.Size = new System.Drawing.Size(195, 23);
            this.bttnRestart.TabIndex = 3;
            this.bttnRestart.Text = "Restart QBO";
            this.bttnRestart.UseVisualStyleBackColor = true;
            this.bttnRestart.Click += new System.EventHandler(this.bttnRestart_Click);
            // 
            // btnRestart
            // 
            this.btnRestart.Location = new System.Drawing.Point(440, 106);
            this.btnRestart.Name = "btnRestart";
            this.btnRestart.Size = new System.Drawing.Size(146, 23);
            this.btnRestart.TabIndex = 2;
            this.btnRestart.Text = "Restart Browser";
            this.btnRestart.UseVisualStyleBackColor = true;
            this.btnRestart.Click += new System.EventHandler(this.btnRestart_Click);
            // 
            // btnCloseBrowser
            // 
            this.btnCloseBrowser.Location = new System.Drawing.Point(440, 52);
            this.btnCloseBrowser.Name = "btnCloseBrowser";
            this.btnCloseBrowser.Size = new System.Drawing.Size(146, 23);
            this.btnCloseBrowser.TabIndex = 1;
            this.btnCloseBrowser.Text = "Close Browser";
            this.btnCloseBrowser.UseVisualStyleBackColor = true;
            this.btnCloseBrowser.Click += new System.EventHandler(this.btnCloseBrowser_Click);
            // 
            // btnShutdownVid
            // 
            this.btnShutdownVid.Location = new System.Drawing.Point(30, 52);
            this.btnShutdownVid.Name = "btnShutdownVid";
            this.btnShutdownVid.Size = new System.Drawing.Size(195, 23);
            this.btnShutdownVid.TabIndex = 0;
            this.btnShutdownVid.Text = "Turn off Video Pi";
            this.btnShutdownVid.UseVisualStyleBackColor = true;
            this.btnShutdownVid.Click += new System.EventHandler(this.btnShutdownVid_Click);
            // 
            // statusStrip1
            // 
            this.statusStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripStatusLabel1});
            this.statusStrip1.Location = new System.Drawing.Point(0, 590);
            this.statusStrip1.Name = "statusStrip1";
            this.statusStrip1.Size = new System.Drawing.Size(1256, 22);
            this.statusStrip1.TabIndex = 1;
            this.statusStrip1.Text = "Mqtt Status:";
            // 
            // toolStripStatusLabel1
            // 
            this.toolStripStatusLabel1.Name = "toolStripStatusLabel1";
            this.toolStripStatusLabel1.Size = new System.Drawing.Size(76, 17);
            this.toolStripStatusLabel1.Text = "MQTT Status:";
            // 
            // WizardForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 15F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1256, 612);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.statusStrip1);
            this.Name = "WizardForm";
            this.Text = "Wizard Of Oz Aplication";
            this.tabControl1.ResumeLayout(false);
            this.tabPage2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarVert)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.trackBarHor)).EndInit();
            this.panel4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tabPage3.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.pictureBoxSnapShot)).EndInit();
            this.panel5.ResumeLayout(false);
            this.panel5.PerformLayout();
            this.panel6.ResumeLayout(false);
            this.panel6.PerformLayout();
            this.tabPage4.ResumeLayout(false);
            this.panel7.ResumeLayout(false);
            this.statusStrip1.ResumeLayout(false);
            this.statusStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.TabPage tabPage1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TabPage tabPage2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RichTextBox txtBoxHtml;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Button btnHtml;
        private System.Windows.Forms.StatusStrip statusStrip1;
        private System.Windows.Forms.ToolStripStatusLabel toolStripStatusLabel1;
        private System.Windows.Forms.ListBox listBoxQuestions;
        private System.Windows.Forms.ListBox listBoxAudio;
        private System.Windows.Forms.RichTextBox txtSpeech;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Button btnSpeak;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioExternal;
        private System.Windows.Forms.RadioButton radioInternal;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TrackBar trackBarVert;
        private System.Windows.Forms.TrackBar trackBarHor;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.TabPage tabPage3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.PictureBox pictureBoxSnapShot;
        private System.Windows.Forms.Panel panel5;
        private System.Windows.Forms.Button btnTestImage;
        private System.Windows.Forms.Button btnStopVid;
        private System.Windows.Forms.Button btnStartVid;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioFemale;
        private System.Windows.Forms.RadioButton radioMale;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.ListView listViewNose;
        private System.Windows.Forms.ColumnHeader columnHeader1;
        private System.Windows.Forms.ListView listViewMouth;
        private System.Windows.Forms.ColumnHeader columnHeader2;
        private System.Windows.Forms.TextBox txtVert;
        private System.Windows.Forms.TextBox txtHor;
        private System.Windows.Forms.Button btnResetAngleHor;
        private System.Windows.Forms.Button btnMoveVert;
        private System.Windows.Forms.Button btnMoveHor;
        private System.Windows.Forms.Panel panel6;
        private System.Windows.Forms.Button btnAudioStop;
        private System.Windows.Forms.Button btnAudioStart;
        private System.Windows.Forms.Label lblVidStatus;
        private System.Windows.Forms.Label lblAudioStatus;
        private System.Windows.Forms.Button btnResetVert;
        private System.Windows.Forms.Button btnMoveRandom;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.TabPage tabPage4;
        private System.Windows.Forms.Panel panel7;
        private System.Windows.Forms.Button btnRestart;
        private System.Windows.Forms.Button btnCloseBrowser;
        private System.Windows.Forms.Button btnShutdownVid;
        private System.Windows.Forms.Button bttnRestart;
        private System.Windows.Forms.Button bttnSurvey;
    }
}

