﻿using AudioRecordMqtt;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using System.Windows.Forms;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;

namespace Wizard
{
    public partial class WizardForm : Form
    {
        private const string MQTT_BROKER_ADDRESS = "87.214.54.242";


        private const string TOPIC_VIDEO_RECORD = "qbo/video/record";
        private const string TOPIC_AUDIO_RECORD = "qbo/audio/record";
        private const string TOPIC_AUDIO_PLAY = "qbo/audio/play";
        private const string TOPIC_AUDIO_SHUTDOWN = "qbo/audio/shutdown";


        private const string TOPIC_SNAPSHOT = "qbo/video/snapshot";
        private const string TOPIC_IMAGE = "qbo/video/image";
        private const string TOPIC_VIDEO_SHUTDOWN = "qbo/video/shutdown";

        private const string TOPIC_VIDEO_STATUS = "qbo/video/status";
        private const string TOPIC_AUDIO_STATUS = "qbo/audio/status";
        private const string TOPIC_WEB_Q = "qbo/web/question";
        private const string TOPIC_WEB_HTML = "qbo/web/html";
        private const string TOPIC_WEB_STOP = "qbo/web/stop";

        private const string TOPIC_HEAD = "qbo/command";


        private MqttClient _client;
        private string[] _audioFiles;
        private string[] _facialExpressions;
        private string[] _noseColors;
        private int _minAngleX;
        private int _maxAngleX;

        private int _minAngleY;
        private int _maxAngleY;
        private int _angleY;
        private int _angleX;

        


        public WizardForm()
        {
            InitializeComponent();
            this.FormClosing += WizzardForm_FormClosing;
            timer1.Tick += MoveHead_Tick;

            ConnectMqtt();
            InitQuestions();
            InitAudioFiles();
            InitExpressions();

            _minAngleX = 290;
            _maxAngleX = 725;

            _minAngleY = 420;
            _maxAngleY = 550;
            _angleX = 511;
            _angleY = _maxAngleY + (_maxAngleY -_minAngleY)/2;
            txtVert.Text = $"{_angleY}";

        }

      
        private void InitQuestions() 
        {

            string[] questions = new string[]
            {
                "Gaat het goed met je?",
                "Kun je mij goed verstaan?",
                "Is deze text leesbaar?",
                "Is dit beter?"

            };

            listBoxQuestions.Items.AddRange(questions);
        }

        private void InitExpressions()
        {

            _facialExpressions = new string[]
            {
                "none",
                "smile",
                "serious",
                "love",
                "sad",
                "surprise",
                "angry",
                "fear","" +
                "disgust"

            };

            _noseColors = new string[]
            {
                "none",
                "blue",
                "green"
            };

            foreach (var clr in _noseColors)
            {
                listViewNose.Items.Add(clr);
            }

            foreach (var expression in _facialExpressions)
            {
                listViewMouth.Items.Add(expression);
            }

        }

        private void InitAudioFiles()
        {

            _audioFiles = new string[]
            {
                "q0_female.wav",
                "q1_female.wav",
                "q2_female.wav",
                "q3_female.wav"

            };

            listBoxAudio.Items.AddRange(_audioFiles);
        }

        private void SendMessage(string message, string topic) 
        {

            try
            {
                if (_client.IsConnected)
                {

                    _client.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message));
                }
                else
                {
                    string clientId = Guid.NewGuid().ToString();
                    _client.Connect(clientId);
                    _client.Publish(topic, System.Text.Encoding.UTF8.GetBytes(message));
                }
            }
            catch (Exception ex) 
            {
                MessageBox.Show(ex.ToString());
            }
        }



        private void WizzardForm_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (_client != null)
            {
                //_client.Unsubscribe(new string[] { TOPIC_IN_RECORD });
                //_client.MqttMsgPublishReceived -= Client_MqttMsgPublishReceived;
                //_client.ConnectionClosed -= Client_ConnectionClosed;
                _client.Disconnect();
                _client = null;
            }
        }

        private void ConnectMqtt() 
        {
            _client = new MqttClient(MQTT_BROKER_ADDRESS);
            _client.MqttMsgPublishReceived += _client_MqttMsgPublishReceived;
            _client.ConnectionClosed += _client_ConnectionClosed;

            string clientId = Guid.NewGuid().ToString();
            _client.Connect(clientId);

            _client.Subscribe(new string[]
                        {
                TOPIC_IMAGE,
                TOPIC_VIDEO_STATUS,
                TOPIC_AUDIO_STATUS
                
                        },
                        new byte[]
                        {
                MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE,
                MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE,
                MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE
       
                        });



            if (_client.IsConnected)
            {
                toolStripStatusLabel1.Text = "Connected with mqtt broker";


            }
        }

        private void _client_ConnectionClosed(object sender, EventArgs e)
        {
           
        }

        private void _client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string topic = e.Topic;

            if (topic.ToLower().Contains(TOPIC_IMAGE))
            {
                try
                {
                    Bitmap bmp = null;
                    using (System.IO.MemoryStream ms = new System.IO.MemoryStream(e.Message))
                    {
                        bmp = new Bitmap(ms);
                    }

                    if (bmp != null)
                    {
                        if (this.InvokeRequired)
                        {
                            this.BeginInvoke(new Action(() => { pictureBoxSnapShot.Image = bmp; }));
                        }
                        else
                            pictureBoxSnapShot.Image = bmp;
                    }
                }
                catch (Exception ex)
                {

                }

                return;
            }


            

            string message = string.Empty;
            try
            {
                message = Encoding.UTF8.GetString(e.Message);
            }
            catch (Exception ex) 
            {

            }

            if (string.IsNullOrEmpty(message)) 
            {
                return;
            }

            if (topic.ToLower().Contains(TOPIC_AUDIO_STATUS))
            {
                InvokeStatusLabel(lblAudioStatus,$"Status: {message}");
            }
            else if (topic.ToLower().Contains(TOPIC_VIDEO_STATUS)) 
            {
                InvokeStatusLabel(lblVidStatus, $"Status: {message}");

            }

        }

        private void InvokeStatusLabel(Label lbl, string txt) 
        {
            if (this.InvokeRequired) 
            {
                this.BeginInvoke(new Action(() => { lbl.Text = txt; }));
            }
            else
                lbl.Text = txt;
        }

        private void btnHtml_Click(object sender, EventArgs e)
        {
            string htmlTxt = txtBoxHtml.Text;

            if (_client != null) 
            {
                if (!_client.IsConnected) 
                {
                    statusStrip1.Text = "Disconnected from mqtt broker!";
                }

                SendMessage(htmlTxt, TOPIC_WEB_HTML);
            }

        }

        private void listBoxQuestions_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = listBoxQuestions.SelectedIndex;
            if (idx < 0)
                return;

            string txt = listBoxQuestions.Items[idx].ToString();

            string audioFile = string.Empty;
            if (_audioFiles != null && idx < _audioFiles.Length)
                audioFile = _audioFiles[idx];

            if (_client != null)
            {
                if (!_client.IsConnected)
                {
                    toolStripStatusLabel1.Text = "Disconnected from mqtt broker!";
                }

                _client.Publish(TOPIC_WEB_Q, System.Text.Encoding.UTF8.GetBytes(txt));

                if (!string.IsNullOrEmpty(audioFile)) 
                {

                    AudioMessage message = new AudioMessage { Filename = audioFile, MessageType = "playfile", OutputType = "internal" };
                    string jsonString = JsonSerializer.Serialize(message);
                   
                    SendMessage(jsonString, TOPIC_AUDIO_PLAY);


                }
            }
        }

        private void btnSpeak_Click(object sender, EventArgs e)
        {

            string speechTxt = txtSpeech.Text;

            string outputType = "external";

            //string liamVoice = "nl-NL_LiamVoice";
            //string emmaVoice = "nl-NL_EmmaVoice";
            string voice = "nl-NL_EmmaVoice";

            if (radioInternal.Checked)
                outputType = "internal";

            if(radioMale.Checked)
                voice = "nl-NL_LiamVoice";

            if (_client != null)
            {
                if (!_client.IsConnected)
                {
                    toolStripStatusLabel1.Text = "Disconnected from mqtt broker!";
                }

                AudioMessage message = new AudioMessage { MessageType = "tts", Text = speechTxt, OutputType = outputType, Voice = voice };
                string jsonString = JsonSerializer.Serialize(message);
                SendMessage(jsonString, TOPIC_AUDIO_PLAY);

            }
        }

        #region Video Recording
        private void btnStartVid_Click(object sender, EventArgs e)
        {
            SendMessage("start", TOPIC_VIDEO_RECORD);

        }

        private void btnStopVid_Click(object sender, EventArgs e)
        {
            SendMessage("stop", TOPIC_VIDEO_RECORD);

        }

        private void btnTestImage_Click(object sender, EventArgs e)
        {
            SendMessage("snapshot", TOPIC_SNAPSHOT);          

        }

        #endregion

        private void listBoxExpressions_SelectedIndexChanged(object sender, EventArgs e)
        {
            int idx = listBoxQuestions.SelectedIndex;
            if (idx < 0)
                return;
        }

        private void listViewMouth_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewMouth.SelectedIndices.Count < 1)
                return;

            int idx = listViewMouth.SelectedIndices[0];

            string expression = _facialExpressions[idx];

            QboCommand cmd = new QboCommand { Command = "Mouth", Expression = expression };
            string jsonString = JsonSerializer.Serialize(cmd);

            SendMessage(jsonString, TOPIC_HEAD);
        }

        private void listViewNose_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (listViewNose.SelectedIndices.Count < 1)
                return;

            int idx = listViewNose.SelectedIndices[0];

            string clr = _noseColors[idx];
            QboCommand cmd = new QboCommand { Command = "Nose", Color = clr };
            string jsonString = JsonSerializer.Serialize(cmd);

            SendMessage(jsonString, TOPIC_HEAD);
        }

        private void trackBar1_Scroll(object sender, EventArgs e)
        {
            int val = trackBarHor.Value;
            SetHorizonalValue(val);
        }

        private void trackBar2_Scroll(object sender, EventArgs e)
        {
            int val = trackBarVert.Value;
            SetVerticalValue(val);
        }

        private void SetVerticalValue(int val) 
        {
            float scaled = val / (float)trackBarVert.Maximum;

            int range = _maxAngleY - _minAngleY;

            int final = _minAngleY + (int)(range * scaled);
            _angleY = final;

            if (_angleY <= _minAngleY)
                _angleY = _minAngleY + 1;

            if (_angleY >= _maxAngleY)
                _angleY = _maxAngleY + 1;

            txtVert.Text = $"{final}";
        }

        private void SetHorizonalValue(int val)
        {
            float scaled = val / (float)trackBarHor.Maximum;

            int range = _maxAngleX - _minAngleX;

            int final = _minAngleX + (int)(range * scaled);
            _angleX = final;

            if (_angleX <= _minAngleX)
                _angleX = _minAngleX + 1;

            if (_angleX >= _maxAngleX)
                _angleX = _maxAngleX + 1;

            txtHor.Text = $"{final}";
        }

        private void btnResetAngles_Click(object sender, EventArgs e)
        {
            trackBarHor.Value = trackBarHor.Maximum / 2 + 1;
            SetHorizonalValue(trackBarHor.Value);
            btnMoveHor_Click(null, null);

        }

        private void btnResetVert_Click(object sender, EventArgs e)
        {
            trackBarVert.Value = trackBarVert.Maximum / 2 + 1;
            SetVerticalValue(trackBarVert.Value);
            btnMoveVert_Click(null, null);

        }

        private void btnMoveHor_Click(object sender, EventArgs e)
        {
            QboCommand cmd = new QboCommand { Command = "Servo", Axis = 1, Angle = _angleX, Speed = 50 };
            string jsonString = JsonSerializer.Serialize(cmd);

            SendMessage(jsonString, TOPIC_HEAD);
        }

        private void btnMoveVert_Click(object sender, EventArgs e)
        {
            QboCommand cmd = new QboCommand { Command = "Servo", Axis = 2, Angle = _angleY, Speed = 50 };
            string jsonString = JsonSerializer.Serialize(cmd);

            SendMessage(jsonString, TOPIC_HEAD);

        }

        private void btnAudioStart_Click(object sender, EventArgs e)
        {
            SendMessage("start", TOPIC_AUDIO_RECORD);

        }

        private void btnAudioStop_Click(object sender, EventArgs e)
        {
            SendMessage("stop", TOPIC_AUDIO_RECORD);

        }

        private void btnMoveRandom_Click(object sender, EventArgs e)
        {
            if (timer1.Enabled) 
            {
                timer1.Enabled = false;
                btnMoveRandom.Text = "Move Head Randomly";
                return;
            }


            timer1.Interval = 5000;
            timer1.Enabled = true;
            btnMoveRandom.Text = "Stop Moving Head Randomly";


        }

        private void MoveHead_Tick(object sender, EventArgs e)
        {
            //move head randomly

            Random rnd = new Random();

            int number = rnd.Next();
            int speed = 50;
            if (number % 2 == 0)
            {
                int max = 6;
                number = rnd.Next(0, max);
                int oldValue = _angleY;

                _angleY = _angleY + (number - max / 2) * 8;
                QboCommand cmd = new QboCommand { Command = "Servo", Axis = 2, Angle = _angleY, Speed = speed };
                string jsonString = JsonSerializer.Serialize(cmd);

                SendMessage(jsonString, TOPIC_HEAD);

                _angleY = oldValue;

            }
            else
            {
                int max = 6;
                number = rnd.Next(0, max);

                int oldValue = _angleX;

                _angleX = _angleX + +(number - max / 2) * 8;
                QboCommand cmd = new QboCommand { Command = "Servo", Axis = 1, Angle = _angleX, Speed = speed };
                string jsonString = JsonSerializer.Serialize(cmd);

                SendMessage(jsonString, TOPIC_HEAD);

                _angleX = oldValue;

            }
        }

        private void btnShutdownVid_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to turn off the Raspberry Pi?", "Are You Sure?", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {

                SendMessage("shutdown", TOPIC_VIDEO_SHUTDOWN);
            }
        }

        private void btnCloseBrowser_Click(object sender, EventArgs e)
        {
            SendMessage("stop", TOPIC_WEB_STOP);
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            SendMessage("reset", TOPIC_WEB_STOP);
        }

        private void bttnRestart_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Do you really want to restart the QBO?", "Are You Sure?", MessageBoxButtons.YesNoCancel) == DialogResult.Yes)
            {

                SendMessage("restart", TOPIC_AUDIO_SHUTDOWN);
            }
        }

        private void bttnSurvey_Click(object sender, EventArgs e)
        {
            SendMessage("survey", TOPIC_WEB_STOP);

        }

        private void listBoxAudio_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
    }
}
