﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioRecordMqtt
{
    public class AudioMessage
    {
        public string MessageType { get; set; }
        public string Text { get; set; }
        public string Filename { get; set; }
        public string OutputType { get; set; }
        public string Voice { get; set; }

    }
}
