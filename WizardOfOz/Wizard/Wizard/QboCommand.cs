﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Wizard
{
    public class QboCommand
    {
        public string Command { get; set; }
        public string Color { get; set; }
        public string Expression { get; set; }
        public string Message { get; set; }
        public string OutputType { get; set; }
        public int Angle { get; set; }
        public int Axis { get; set; }
        public int Speed { get; set; }
    }
}
