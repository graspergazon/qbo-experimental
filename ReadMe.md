### MQTT

**prerequisites**  
Install paho-mqtt for user qbo: 
```
sudo -u qbo pip install paho-mqtt  
```

Copy Python/QboMqttListener.py to /opt/qbo  
run script by calling:
```
sudo -u qbo python /opt/qbo/QboMqttListener.py
```

---


### Web Interface
The web interface can run on any computer connected to the internet, since the communication between qbo and web interface (backend) is over MQTT. 
The backend of the Web Interface is a simple .Net 5.0 C# application which communicates with the browser over a websocket.

**prerequisites**  
Install .Net Core SDK 5.0  
For Raspberry Pi you can use [these instructions](https://www.petecodes.co.uk/install-and-use-microsoft-dot-net-5-with-the-raspberry-pi)

### Backend
**Howto build**  

#### Build script 
```
cd Scripts  
./buildAlldotnet.sh
``` 
This will build all dotnet projects in Release and copy the files to   
/home/pi/vv/build/Audio and /home/pi/vv/build/WebSocket  
The scripts are copied to /home/vv/scripts 

#### Build manually 
```
cd Websocket/SimpleWebsocketServer  
dotnet build -c Release  
dotnet run --no-build
```


#### Frontend
Just open web/dashboard.html with a browser that supports web sockets

---


### Auto Startup
We use cron jobs to automatically start scripts.  
Add to /etc/crontab:
```
@reboot pi /bin/bash /home/pi/vv/scripts/startHead.sh >> /home/pi/vv/cron.log 2>&1
@reboot pi /bin/bash /home/pi/vv/scripts/startAudio.sh >> /home/pi/vv/cron.log 2>&1
```  
  
The websocket server automatically starts a browser. This cannot be done in a cron job so we add  
```
"@/bin/bash /home/pi/scripts/startWsServer.sh"
```
to /home/pi/.config/lxsession/LXDE-pi/autostart

---


### Wizard of Oz
The Wizard of Oz application runs only on Windows.
It is a .Net 5 Winforms application and can be built using the command line:  
```
cd WizardOfOz/Wizard/Wizard
dotnet build -c Release  
dotnet run --no-build
```  
The best way to edit the application is to use Visual Studio 2019 or Visual Studio 2022