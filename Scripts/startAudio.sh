#!/bin/bash
echo "Waiting for 5 seconds..."
sleep 5
echo "Checking internet connection"
internetStatus=$(curl -s -I https://www.google.com/ | grep "HTTP/2 200")
while [ -z "$internetStatus" ]
do
    echo "No internet connection. Trying again in 10 seconds"
    sleep 10
    internetStatus=$(curl -s -I https://www.google.com/ | grep "HTTP/2 200")
done
    echo "QBO is Connected to the internet"



CHECK=`ps -aux | grep -v grep | grep AudioRecordMqtt`
if [ -z "$CHECK" ]; then
    ISRUNNING=false
    echo "Is not running"
else
    ISRUNNING=true
    echo "Is running"

fi

echo "IsRunning has value $ISRUNNING"

if $ISRUNNING = true; then
	echo "AudioRecordMqtt is already running"
else
	echo "launching AudioRecordMqtt"
	dotnet /home/pi/vv/build/Audio/AudioRecordMqtt.dll
fi