#!/bin/bash
echo "Waiting for 15 seconds..."
sleep 15
echo "Checking internet connection"
internetStatus=$(curl -s -I https://www.google.com/ | grep "HTTP/2 200")
while [ -z "$internetStatus" ]
do
    echo "No internet connection. Trying again in 10 seconds"
    sleep 10
    internetStatus=$(curl -s -I https://www.google.com/ | grep "HTTP/2 200")
done
    echo "QBO is Connected to the internet"



CHECK=`ps -aux | grep -v grep | grep MqttCommand`
if [ -z "$CHECK" ]; then
    ISRUNNING=false
    echo "Is not running"
else
    ISRUNNING=true
    echo "Is running"

fi

echo "IsRunning has value $ISRUNNING"

if $ISRUNNING = true; then
	echo "MqttCommand.py is already running"
else
	echo "launching MqttCommand.py"
		sudo -u qbo python /opt/qbo/MqttCommand.py >> /home/pi/logs/mqttPython.log 2>&1
fi


#sudo -u pi /usr/local/bin/dotnet /home/pi/vv/Video/bin/VideoRecordMqtt.dll
