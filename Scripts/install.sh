#!/bin/bash
#sudo apt update
#sudo apt install git -y

BASEDIR=/home/pi/vv
if [ -d "$BASEDIR" ]; then
    echo "basedir exists"
else
    mkdir /home/pi/vv    
fi

LOGDIR=$/home/pi/logs
if [ -d "$LOGDIR" ]; then
    echo "log dir exists"
else
    mkdir /home/pi/logs    
fi

GITDIR=/home/pi/vv/firstpilot
if [ -d "$GITDIR" ]; then
    echo "basedir exists"
else
    git clone  https://gitlab.com/vicarvision/grants/aid2bewell/firstpilot.git     
fi

MQTTFILE=/opt/qbo/MqttCommand.py
if [ -f "$MQTTFILE" ]; then
    echo "$MQTTFILE exists"
else
    sudo cp $GITDIR/Python/MqttCommand.py /opt/qbo
fi
