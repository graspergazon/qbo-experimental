﻿using Serilog;
using System;
using System.IO;
using System.Runtime.CompilerServices;
using System.Runtime.InteropServices;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleWebSocketServer
{
    class Program
    {
        public const int CLOSE_SOCKET_TIMEOUT_MS = 2500;

        // async Main requires C# 7.2 or newer in csproj properties
        static async Task Main(string[] args)
        {
            var exitEvent = new ManualResetEvent(initialState: false);
            AppDomain.CurrentDomain.ProcessExit += (o, e) => exitEvent.Set();

            InitLogger();

            try
            {
                WebSocketServer.MqttConnect("87.214.54.242", 1883);

                WebSocketServer.Start("http://localhost:8080/");
                Console.WriteLine("Press any key to exit...\n");
                exitEvent.WaitOne();
                await WebSocketServer.StopAsync();
            }
            catch(OperationCanceledException)
            {
                // this is normal when tasks are canceled, ignore it
            }

            // VS2019 prompts to close the console window by default
        }

        public static void ReportException(Exception ex, [CallerMemberName]string location = "(Caller name not set)")
        {
            Console.WriteLine($"\n{location}:\n  Exception {ex.GetType().Name}: {ex.Message}");
            if (ex.InnerException != null) Console.WriteLine($"  Inner Exception {ex.InnerException.GetType().Name}: {ex.InnerException.Message}");
        }

        private static void InitLogger()
        {
            string logDir = "Logs";


            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                logDir = "/home/pi/logs";
            }

            try
            {
                if (!Directory.Exists(logDir))
                {
                    Directory.CreateDirectory(logDir);
                }
            }
            catch (Exception ex)
            { }

            Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.File(Path.Combine(logDir, "WsLog.txt"), rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information)
                 .WriteTo.File(Path.Combine(logDir, "WsErrorLog.txt"), rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error)
                 .CreateLogger();
        }
    }
}