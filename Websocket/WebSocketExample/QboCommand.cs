﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleWebSocketServer
{
    public class QboCommand
    {
        public string Command { get; set; }
        public string Color { get; set; }
        public string Expression { get; set; }
        public string Message { get; set; }
        public string OutputType { get; set; }
        public int Angle { get; set; }
        public int Axis { get; set; }
        public int Speed { get; set; }

        public int Delay { get; set; }
    }

    public class DashBoardMessage
    {
        public string MessageType { get; set; }
        public string ImageData { get; set; }
        public string Info { get; set; }
        public string Status { get; set; }
    }

    public class AudioMessage
    {
        public string MessageType { get; set; }
        public string Text { get; set; }
        public string Filename { get; set; }
        public string OutputType { get; set; }
        public string Voice { get; set; }
        public int Delay { get; set; }

    }
}
