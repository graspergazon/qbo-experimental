let uitlegE = "Ik ga een aantal emoties tonen, kunt u deze nadoen?"
let expression = ['neutraal','blij', 'verdrietig', 'verrast', 'boos', 'bang', 'walging'];
let command = ['none', 'smile', 'sad', 'surprise', 'angry', 'fear', 'disgust'];
let idx = 0;
//consts
const nxt = document.getElementById('btn nxt');

function showExpression (idx) {
    document.getElementById("emotie").style.marginTop = "10%";
    document.getElementById("emotie").innerHTML = expression[idx];
    sendMouth(command[idx]);
}

function nxt_qst(){

    //if (idx == -1) document.getElementById("emotie").innerHTML = uitlegE;

    // gestelde vraag
    if(idx>expression.length-1)
    {
     showEnd();
     return;
    }

    showExpression(idx);

    idx = idx+1;
}

function showEnd()
{
    document.getElementById("emotie").innerHTML = "Einde";
    nxt.style.backgroundColor= '#ba1600';
    nxt.innerHTML = "Herstart";

    //RequestRecording("audio", "stop")
    //RequestRecording("video", "stop")

    nxt.onclick = function()
    {location.href = 'Start.html';
    }
}