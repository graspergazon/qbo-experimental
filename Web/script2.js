function myFunction() {
    document.getElementById("status").innerHTML = "Hello Dear Visitor!</br> We are happy that you've chosen our website to learn programming languages. We're sure you'll become one of the best programmers in your country. Good luck to you!";
  }


  var wsUri = "ws://localhost:8080";
//   var wsUri = "ws://localhost:51717";
  var output;
  var websocket;
  
  function init()
  {
      output = document.getElementById("output");
      configWebSocket();
  }
  
  function sendHelp() {
    sendTextFrame("help");
  }

  function configWebSocket()
  {
      websocket = new WebSocket(wsUri);
      websocket.binaryType = 'arraybuffer';
      websocket.onopen = function(evt) { onOpen(evt) };
      websocket.onclose = function(evt) { onClose(evt) };
      websocket.onmessage = function(evt) { onMessage(evt) };
      websocket.onerror = function(evt) { onError(evt) };
  }

  function sendTextFrame(message)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        emit("SENT: " + message);
        websocket.send(message);
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}
  
function sendMouth(emotion)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Mouth", Expression: emotion};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function sendServo(axis, angle, speed)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Servo", Axis: axis, Angle: angle, Speed: speed};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function sendMoveRelative(axis, angle)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "MoveRel", Axis: axis, Angle: angle};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function sendNose(color)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Nose", Color: color};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

  function onOpen(evt)
  {


      var txt = "<div class='alert alert-success' role='alert'>Connected to QBO</div>"
        document.getElementById("status").innerHTML = txt;

    
    //   emit(txt);
      
  }
  
  function onClose(evt)
  {


      var txt = "<div class='alert alert-danger' role='alert'>Disconnected!</div>"
      document.getElementById("status").innerHTML = txt;
    //   emit(txt);
  }
  
  function onMessage(message)
  {
      emit(message.data)
//        var c = document.getElementById('mycanvas');
//   var ctx = c.getContext('2d');
//   var imageData = ctx.createImageData(320, 240);
//   var pixels = imageData.data;
  
//   var buffer = new Uint8Array(message.data);
//   for (var i=0; i < pixels.length; i++) {
//   pixels[i] = buffer[i];
//   }
//   ctx.putImageData(imageData, 0, 0);
  
      
//   var p = document.getElementById('imagaDataLen');
//   p.textContent = pixels.byteLength + ' bytes';
  }
  
  function onError(evt)
  {
      var txt = "<div class='alert alert-warning' role='alert'>" + evt.data + "</div>"
      emit(txt);
  }

  function emit(message)
  {
      var pre = document.createElement("p");
      // pre.style.wordWrap = "break-word";
      pre.innerHTML = message;
      output.appendChild(pre);
  }

  window.addEventListener("load", init, false);
