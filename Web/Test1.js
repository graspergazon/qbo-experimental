// let speech = ["Is het mooi weer", 
//                 "Ben je vandaag buiten geweest?", 
//                 "Is het cafe gezellig?"];
// let text = ["Heb je gister iets leuks gedaan?", 
//                 "Beweeg je graag?", 
//                 "Heb je vandaag al geluncht?"];
let uitleg = "Ik ga een aantal vragen stellen."; // Ik ga u een aantal vragen stellen. Klik op volgende om te beginnen.
let questions = ["Is het mooi weer", 
                "Ben je vandaag buiten geweest?", 
                "Is het cafe gezellig?",
                "Heb je gister iets leuks gedaan?", 
                "Beweeg je graag?", 
                "Heb je vandaag al geluncht?"];     // 2 vragen -> 
// let answer = ["Geef antwoord door ja of nee te zeggen.", 
//                 "Geef antwoord via de knoppen op de tablet.",
//                 "Raak voor ja de linkerkant van mijn hoofd aan en voor nee de rechterkant."];   // "U kunt nu zelf kiezen hoe u antwoord wilt geven."
let answer = ["Geef antwoord door ja of nee te zeggen.", "Geef antwoord door ja of nee te zeggen.",
                "Raak voor ja de linkerkant van mijn hoofd aan en voor nee de rechterkant.", "Raak voor ja de linkerkant van mijn hoofd aan en voor nee de rechterkant.", 
                "U kunt nu zelf kiezen hoe u antwoord geeft.", "U kunt nu zelf kiezen hoe u antwoord geeft."];      // audiofile!
let reacYes = ["Dat is fijn!",
            "Dat is lekker!",
            "Dat vind ik ook!",
            "Dat is leuk!",
            "Dat is gezond!",
            "Dat is lekker!"]
let reacNo = ["Dat is jammer!",
            "Misschien straks nog even.",
            "Dat is jammer.",
            "Een andere keer.",
            "Ik beweeg ook niet veel.",
            "Eet smakelijk alvast!"]
let exprYes = ["smile", "smile", "smile", "smile", "smile", "smile"]
let exprNo = ["sad", "smile", "sad", "neutral", "smile", "smile"]
let colorBut = ["#ffffff", "#ffffff", "#ffffff"];
// background-color: #008CBA
let colIdx = 0; //-1
let questionIdx = 0;    //-1
let question = questions[questionIdx];
let colorTxt = ['black', 'white'];
// consts
const ans1 = document.getElementById('btn ans1');
const ans2 = document.getElementById('btn ans2');
const nxt = document.getElementById('btn nxt');
let voice = "male";

function colors (clicked, unclicked) {
    clicked.style.backgroundColor = '#024b66';
    clicked.style.color = 'white';
    unclicked.style.backgroundColor = '#6fdafc';
    unclicked.style.color = 'black';
}

function playAudio(voice, file)
{
    let audioFile = file;
    requestPlayAudioFile(audioFile, "external");
}

function robotReaction (reacYN, exprYN) {
    // speech
    // document.getElementById('txt reaction').innerHTML = reacYN[questionIdx];    //print controle
    //requestSpeakWithArg(reacYN[questionIdx]);
    
   

    // expression
    // document.getElementById('txt expres').innerHTML = exprYN[questionIdx];      //print controle
    sendMouth(exprYN[questionIdx]);
}

// clicked answer 1
ans1.addEventListener('click', function onClick() {

    //colors(ans1, ans2);

    robotReaction(reacYes, exprYes);
    let idx = questionIdx-1;
    let file = "Other_ans1_" + idx + "_";
    playAudio(voice, file)
});

// clicked answer 2
ans2.addEventListener('click', function onClick() {

    //colors(ans2, ans1);

    robotReaction(reacNo, exprNo);
    let idx = questionIdx-1;

    let file = "Other_ans2_" + idx + "_";
    playAudio(voice, file)
});

function questionInstruction (questionIdx, colIdx) {
    // let speak = (questions[questionIdx] + answer[questionIdx]);
    // requestSpeakWithArg(speak); 
}

function askQuestion (questionIdx, colorTxt) {
    //requestSpeakWithArg(questions[questionIdx]);
    document.getElementById("qst").innerHTML = questions[questionIdx];
    document.getElementById("qst").style.color = colorTxt;

    let file = "Other_q_" + questionIdx + "_";
    playAudio(voice, file)
}

function giveInstruction (colIdx, colorTxt) {
    //requestSpeakWithArg(answer[questionIdx]);
    document.getElementById("ans").innerHTML = answer[questionIdx];
    document.getElementById("ans").style.color = colorTxt[colIdx];
    document.getElementById("ans").style.fontSize = "30px";
}

function visibility (button, YN) {
    button.style.backgroundColor = colorBut[colIdx];
    button.style.color = 'white';
    button.innerHTML = YN;
}

// function end () {
//     document.getElementById("btn nxt").innerHTML = <a href="Start.html"><button class="button option" id="btn survey">Einde</button></a>;
// }

function nxt_qst(){
    // gestelde vraag
    
    if(questionIdx>questions.length-1)
    {
        showEnd();
        return;
    }

    // padding vd antwoorden
    if (colIdx == 2) colIdx = 0;

    //if (questionIdx>2) colorTxt = 'black';

    // uitleg van de qbo
    // if (questionIdx=-1) {
    //     let file = "xx" + idx + "_";        // dit gaat waarschijnlijk niet goed, hoe heet de file die zegt: "Ik ga een aantal vragen stellen."
    //     playAudio(voice, file);
    // }

    questionInstruction(questionIdx, colIdx);
    askQuestion(questionIdx, colorTxt);
    giveInstruction(colIdx, colorTxt);

    // button 1: Ja
    //visibility(ans1, "Ja");   // "btn ans1"
    // button 2: Nee
    //visibility(ans2, "Nee");   // "btn ans2"
    document.body.style.background = 'white';

    questionIdx = questionIdx+1;
    colIdx = colIdx+1;

}

function showEnd()
{
    document.getElementById("qst").innerHTML = "Einde";
    document.getElementById("ans").innerHTML = "";
    nxt.style.backgroundColor= '#ba1600';
    ans1.style.display = "none";
    ans2.style.display = "none";
    nxt.innerHTML = "Herstart";

    // RequestRecording("audio", "stop")
    // RequestRecording("video", "stop")

    nxt.onclick = function(){location.href = 'Start.html';
    }
}
