let uitleg1 = "Ik ga een aantal vragen stellen."; 
let uitleg2 = "Geef antwoord via de knoppen op de tablet.";
let questions = ["Heeft u een smartphone?", 
                "Praat je graag met mij?", 
                "Zou u het gezellig vinden als ik in uw woonkamer zou staan?", 
                "Zie ik er goed uit vandaag?", 
                "Luistert u graag naar muziek?", 
                "Wilt u een mop horen?"];

                
let reac1 = ["Dat is handig!",
            "Dat vind ik leuk!",
            "Ik ook!",
            "Dankjewel!",
            "Ik ook!",
            "Wat is het lievelings muziekgenre van een robot? Heavy metal."]
let reac2 = ["Lekker rustig.",
            "Dat vind ik jammer.",
            "Dat vind ik jammer.",
            "Ik heb mijn best gedaan.",
            "Ik ook niet.",
            "Dat is goed!"]
let expr1 = ["smile", "love", "love", "love", "smile", "smile"]
let expr2 = ["smile", "sad", "sad", "sad", "neutral", "neutral"]

let pad = ["28px 58px", "28px 330px", "90px 70px"];
let mar = ["", "50px", ""];



// padding: 28px 58px;
let padIdx = 0;
let questionIdx = 0;
let question = questions[questionIdx];

let voice = "male"; //or female
let hasVoiceSelected = 0;

//const green = document.getElementById('btn ans1');

const ans1 = document.getElementById('btn ans1');
const ans2 = document.getElementById('btn ans2');
const nxt = document.getElementById('btn nxt');

function clickAns1()
{

    // if(!hasVoiceSelected || questionIdx == 0)
    // {
    //     voice = "male";
    //     playAudio(voice, "VoiceSelect_ans_");
    //     hasVoiceSelected = 1;

    //     return;
    // }

    ans1.style.backgroundColor = '#024b66';
    ans1.style.color = 'white';
    ans2.style.backgroundColor = '#6fdafc';
    ans2.style.color = 'black';
    let idx = questionIdx-1;

    // reactie vd robot
    let txt = reac1[idx];
    //document.getElementById('txt reaction').innerHTML = txt;
    let file = "Survey_ans1_" + idx + "_";
    playAudio(voice, file)
    //requestTTS(txt, "external");    

    let expression = expr1[idx];
    sendMouth(expression);
}

function clickAns2()
{
    // if(!hasVoiceSelected || questionIdx == 0)
    // {
    //     voice = "female";
    //     playAudio(voice, "VoiceSelect_ans_");
    //     hasVoiceSelected = 1;
    //     return;
    // }

    ans2.style.backgroundColor = '#024b66'; //036082
    ans2.style.color = 'white';
    ans1.style.backgroundColor = '#6fdafc'; //02b7f2    2ac7fa
    ans1.style.color = 'black';

    let idx = questionIdx-1;
    // reactie vd robot
    let txt = reac2[idx];
    //document.getElementById('txt reaction').innerHTML = txt;

    //requestTTS(txt, "external");    
    let file = "Survey_ans2_" + idx + "_";
    playAudio(voice, file)
    // document.getElementById('txt expres').innerHTML = expr2[questionIdx];
    let expression = expr2[idx];
    sendMouth(expression);
}

function playAudio(voice, file)
{
    let audioFile = file;
    requestPlayAudioFile(audioFile, "external");
}

function nxt_qst(){
    // gestelde vraag
    
    // if(questionIdx<0)
    // {
    //  showStart();
    //  return;
    // }

    if(questionIdx>questions.length-1)
       {
        showEnd();
        return;
       }

    // padding vd antwoorden
    if (padIdx == 3) padIdx = 0;

    // uitleg van de qbo
    // if (questionIdx=-1) {
    //     let file = "xx" + idx + "_";        // dit gaat waarschijnlijk niet goed, hoe heet de file die zegt: "Ik ga een aantal vragen stellen." = uitleg1
    //     playAudio(voice, file);
    //     let file = "xx" + idx + "_";        // dit gaat waarschijnlijk niet goed, hoe heet de file die zegt: "Geef antwoord via de knoppen op de tablet." = uitleg2
    //     playAudio(voice, file);
    // }

    /* live aanroepen? */

    // button 1: Ja
    document.getElementById("qst").innerHTML = questions[questionIdx];
    
    let file = "Survey_q_" + questionIdx + "_";
    playAudio(voice, file)
    
    document.getElementById("btn ans1").style.backgroundColor = '#008CBA';
    document.getElementById("btn ans1").style.color = 'white';
    document.getElementById("btn ans1").style.padding = pad[padIdx];
    document.getElementById("btn ans1").innerHTML = "Ja";
    // button 2: Nee
    document.getElementById("btn ans2").style.backgroundColor = '#008CBA';
    document.getElementById("btn ans2").style.color = 'white';
    document.getElementById("btn ans2").style.padding = pad[padIdx];
    document.getElementById("btn ans2").style.marginTop = mar[padIdx];


    document.getElementById("btn ans2").innerHTML = "Nee";
    document.body.style.background = 'white';

    questionIdx = questionIdx+1;
    padIdx = padIdx+1;

    

}

function showStart()
{
    document.getElementById("qst").innerHTML = "U kunt nu een stem voor mij kiezen.";
    nxt.style.backgroundColor= 'gray';   // #ba1600
    
     // button 1: Ja
     document.getElementById("btn ans1").style.backgroundColor = '#008CBA';
     document.getElementById("btn ans1").style.color = 'white';
     document.getElementById("btn ans1").style.padding = "28px 50px";
     document.getElementById("btn ans1").innerHTML = "Mannelijk";
     // button 2: Nee
     document.getElementById("btn ans2").style.backgroundColor = '#008CBA';
     document.getElementById("btn ans2").style.color = 'white';
     document.getElementById("btn ans2").style.padding = "28px 50px";
 
     document.getElementById("btn ans2").innerHTML = "Vrouwelijk";
     document.body.style.background = 'white';
 
     questionIdx = 0;
     padIdx = 0;

     playAudio("male", "VoiceSelect_q_");
}

function showEnd()
{
    document.getElementById("qst").innerHTML = "Einde";
    nxt.style.backgroundColor= '#ba1600';
    ans1.style.display = "none";
    ans2.style.display = "none";
    nxt.innerHTML = "Herstart";

    // RequestRecording("audio", "stop")
    // RequestRecording("video", "stop")

    nxt.onclick = function()
    {
        location.href = 'Start.html';
    }
}