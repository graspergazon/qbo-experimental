function myFunction() {
    document.getElementById("status").innerHTML = "Hello Dear Visitor!</br> We are happy that you've chosen our website to learn programming languages. We're sure you'll become one of the best programmers in your country. Good luck to you!";
  }


  var wsUri = "ws://localhost:8080";
//   var wsUri = "ws://localhost:51717";
  var output;
  var websocket;
  var rangePicker1;

  function init()
  {
      //image_output
      output = document.getElementById("output");
      configWebSocket();      
  }
  
function onSlideHorizontal(evt)
{

}

function RequestRecording(recordingType, command)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Record", Message: recordingType + ", " + command};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function setHeadAngleHorizontal(evt)
{
    let slider = document.getElementById("sliderHz");
    let val = slider.value;
    let min = 295;
    let max = 720;
    let range = max-min;

    let angle = min + Math.round(range* (val/100));

    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Servo", Axis: 1, Angle: angle, Speed: 50};
       
        websocket.send(JSON.stringify(obj));
    }

//sliderHz
}

function setHeadAngleVertical(evt)
{
    let slider = document.getElementById("sliderVert");
    let val = slider.value;
    let min = 420;
    let max = 550;
    let range = max-min;

    let angle = min + Math.round(range* (val/100));

    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Servo", Axis: 2, Angle: angle, Speed: 50};
       
        websocket.send(JSON.stringify(obj));
    }

//sliderHz
}


  function sendHelp() {
    sendTextFrame("help");
  }

  function configWebSocket()
  {
      websocket = new WebSocket(wsUri);
      websocket.binaryType = 'arraybuffer';
      websocket.onopen = function(evt) { onOpen(evt) };
      websocket.onclose = function(evt) { onClose(evt) };
      websocket.onmessage = function(evt) { onMessage(evt) };
      websocket.onerror = function(evt) { onError(evt) };
  }

  function sendTextFrame(message)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        emit("SENT: " + message);
        websocket.send(message);
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}
  
function requestSpeak()
{
    let txtElement = document.getElementById("txtToSpeak");
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Speak", Message: txtElement.value};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function requestSpeakWithArg(text)
{
    // let txtElement = document.getElementById("txtToSpeak");
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Speak", Message: text};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function playAudio(outputType)
{ 
    let selection = document.getElementById("audioSelect");

    let selectedFile = selection.value;
    
    playAudioFile(selectedFile, outputType)

}

function playAudioFile(filename, outputType)
{  
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "PlayAudio", Message: filename, OutputType: outputType};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function shutdownVideo()
{  
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Shutdown", Message: "Video"};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function requestSnapShot()
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Snapshot"};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}



function showImage(message)
{
    // let message = JSON.parse(evt.data); 

    let image = message.ImageData;

    let imageStr = "<img src='" + image + "'>";

    // let imageStr = "";
    // for (let i = 0; i < image.length; i++) {
    //     imageStr += "<img src='" + image[i] + "'>";
    // } 

    let pre = document.createElement("p");   
    // pre.innerHTML = <br><h4>Matching Score: " + message.Score+ "</h4>";   
    let txt = "<div class='card' style='width: 700px;'><div class='card-body'><p class='card-text'>" + message.Info + "</p>"+ imageStr +"</div></div>";
    pre.innerHTML = txt;
    //output.appendChild(pre);
     //image_output
     imageOut = document.getElementById("image_output");
     imageOut.prepend(pre);
}




  function onOpen(evt)
  {


      var txt = "<div class='alert alert-success' role='alert'>Connected to QBO</div>"
        document.getElementById("status").innerHTML = txt;

    
    //   emit(txt);
      
  }
  
  function onClose(evt)
  {


      var txt = "<div class='alert alert-danger' role='alert'>Disconnected!</div>"
      document.getElementById("status").innerHTML = txt;
    //   emit(txt);
  }
  
  function onMessage(evt)
  {
    let message = JSON.parse(evt.data); 

    if(message.MessageType == 'Image')
    {
        showImage(message);
    }
    else if(message.MessageType == 'Status')
    {
        ShowStatus(message.Status);
    }

      //emit(message.data)
//        var c = document.getElementById('mycanvas');
//   var ctx = c.getContext('2d');
//   var imageData = ctx.createImageData(320, 240);
//   var pixels = imageData.data;
  
//   var buffer = new Uint8Array(message.data);
//   for (var i=0; i < pixels.length; i++) {
//   pixels[i] = buffer[i];
//   }
//   ctx.putImageData(imageData, 0, 0);
  
      
//   var p = document.getElementById('imagaDataLen');
//   p.textContent = pixels.byteLength + ' bytes';
  }
  
  function onError(evt)
  {
      var txt = "<div class='alert alert-warning' role='alert'>" + evt.data + "</div>"
      emit(txt);
  }

  function ShowStatus(statusTxt)
  {
      var txt = "<div class='alert alert-dark' role='alert'>" + statusTxt + "</div>"
      document.getElementById("statusAlt").innerHTML = txt;
     
  }

  function emit(message)
  {
      var pre = document.createElement("p");
      // pre.style.wordWrap = "break-word";
      pre.innerHTML = message;
      output.appendChild(pre);
  }

  window.addEventListener("load", init, false);
