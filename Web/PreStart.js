

function ShowOptions()
{
    let element = document.getElementById("start");
    element.setAttribute("hidden", "hidden");

    let rec = document.getElementById("record");
    rec.setAttribute("hidden", "hidden");    

    // ans1.style.padding = "15px 40px"
    // ans2.style.padding = "15px 40px"

    let voiceBtns = document.getElementById("voiceBtn");
    voiceBtns.removeAttribute("hidden");

    document.getElementById("qst").innerHTML = "U kunt nu een stem voor mij kiezen.";
    
    let start = document.getElementById("startExperiment");
    start.removeAttribute("hidden");

    startRecording();
}


function startRecording()
{
    let recAudio = document.getElementById("checkAudioRec");
    let recVideo = document.getElementById("checkVideoRec");

    if(recAudio.checked)
    {
        console.log("Recording audio");
        RequestRecording("audio", "start")

    }
    else
    {
        console.log("Recording audio");

    }

    if(recVideo.checked)
    {        
        console.log("Recording audio");
        RequestRecording("video", "start")
        
    }
    else
    {
        console.log("Not recording video");
    }
}

function playAudio(voice, file)
{
    let audioFile = file + voice + ".wav";
    requestPlayAudioFile(audioFile, "external");
}

function SelectMaleVoice()
{
    let voice = "male";
    setVoice(voice);
    playAudio(voice, "VoiceSelect_ans_");
}

function SelectFeMaleVoice()
{
    let voice = "female";
    setVoice(voice);
    playAudio(voice, "VoiceSelect_ans_");
}

function StartExperiment()
{
    

    location.href = 'Start.html'
}