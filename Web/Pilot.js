let questions = ["Ben je vandaag al buiten geweest?", "Was het lekker warm buiten?", "Heb je al gegeten?", "Was het eten lekker?"];
let questionIdx = 0;

  var wsUri = "ws://localhost:8080";
//   var wsUri = "ws://localhost:51717";
  var output;
  var websocket;
  

  function init()
  {
      //image_output
      output = document.getElementById("output");
      configWebSocket();      

      questionIdx=0;
      let question = questions[questionIdx];

        document.getElementById("question").innerHTML = "<h3>" + question + "</h3>";
  }
  



  function configWebSocket()
  {
      websocket = new WebSocket(wsUri);
      websocket.binaryType = 'arraybuffer';
      websocket.onopen = function(evt) { onOpen(evt) };
      websocket.onclose = function(evt) { onClose(evt) };
      websocket.onmessage = function(evt) { onMessage(evt) };
      websocket.onerror = function(evt) { onError(evt) };
  }

  function sendTextFrame(message)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        emit("SENT: " + message);
        websocket.send(message);
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}
  


  function onOpen(evt)
  {


      var txt = "<div class='alert alert-success' role='alert'>Connected to QBO</div>"
        document.getElementById("status").innerHTML = txt;

    
    //   emit(txt);
      
  }
  
  function onClose(evt)
  {


      var txt = "<div class='alert alert-danger' role='alert'>Disconnected!</div>"
      document.getElementById("status").innerHTML = txt;
    //   emit(txt);
  }
  
  function nextQuestion()
  {
        questionIdx = questionIdx+1;

        if(questionIdx>questions.length-1)
        questionIdx=questions.length-1;

        let question = questions[questionIdx];

        document.getElementById("question").innerHTML = "<h3>" + question + "</h3>";

  }

  function prevQuestion()
  {
        questionIdx = questionIdx-1;
        if(questionIdx<0)
            questionIdx=0;

        let question = questions[questionIdx];

        document.getElementById("question").innerHTML = "<h3>" + question + "</h3>";
        

  }

  function showQuestion(questionTxt)
  {
    document.getElementById("question").innerHTML = "<h3>" + questionTxt + "</h3>";
  }

  function showHtml(htmlText)
  {
    document.getElementById("question").innerHTML = htmlText;
  }


  function onMessage(evt)
  {
    let message = JSON.parse(evt.data); 

    if(message.MessageType == 'Image')
    {
        showImage(message);
    }
    else if(message.MessageType == 'Status')
    {
        ShowStatus(message.Status);
    }
    else if(message.MessageType == 'Text')
    {
        showQuestion(message.Info);
    }
    else if(message.MessageType == 'HTML')
    {
        showHtml(message.Info);
    }

      //emit(message.data)
//        var c = document.getElementById('mycanvas');
//   var ctx = c.getContext('2d');
//   var imageData = ctx.createImageData(320, 240);
//   var pixels = imageData.data;
  
//   var buffer = new Uint8Array(message.data);
//   for (var i=0; i < pixels.length; i++) {
//   pixels[i] = buffer[i];
//   }
//   ctx.putImageData(imageData, 0, 0);
  
      
//   var p = document.getElementById('imagaDataLen');
//   p.textContent = pixels.byteLength + ' bytes';
  }
  
  function onError(evt)
  {
      var txt = "<div class='alert alert-warning' role='alert'>" + evt.data + "</div>"
      emit(txt);
  }

  function ShowStatus(statusTxt)
  {
      var txt = "<div class='alert alert-dark' role='alert'>" + statusTxt + "</div>"
      document.getElementById("statusAlt").innerHTML = txt;
     
  }

  function emit(message)
  {
      var pre = document.createElement("p");
      // pre.style.wordWrap = "break-word";
      pre.innerHTML = message;
      output.appendChild(pre);
  }

  window.addEventListener("load", init, false);
