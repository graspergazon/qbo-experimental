var wsUri = "ws://localhost:8080";

var websocket;

  function init()
  {
      //image_output
      //output = document.getElementById("output");
      configWebSocket();      
  }

  function configWebSocket()
  {
      websocket = new WebSocket(wsUri);
      websocket.binaryType = 'arraybuffer';
      websocket.onopen = function(evt) { onOpen(evt) };
      websocket.onclose = function(evt) { onClose(evt) };
      websocket.onmessage = function(evt) { onMessage(evt) };
      websocket.onerror = function(evt) { onError(evt) };
  }

  function onOpen(evt)
  {


      var txt = "<div class='alert alert-success' role='alert'>Connected to QBO</div>"
        //document.getElementById("status").innerHTML = txt;

    
    //   emit(txt);
      
  }
  
  function onClose(evt)
  {


      var txt = "<div class='alert alert-danger' role='alert'>Disconnected!</div>"
      document.getElementById("status").innerHTML = txt;
    //   emit(txt);
  }
  
  function onMessage(evt)
  {
    let message = JSON.parse(evt.data); 
    
  }
  
  function onError(evt)
  {
      var txt = "<div class='alert alert-warning' role='alert'>" + evt.data + "</div>"
      document.getElementById("status").innerHTML = txt;
  }

  function requestTTS(txt, outputType)
{
    // let txtElement = document.getElementById("txtToSpeak");
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Speak", Message: txt, OutputType: outputType};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function requestPlayAudioFile(filename, outputType)
{  
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "PlayAudio", Message: filename, OutputType: outputType, Delay: 1000};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function setVoice(voice)
{  
    
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "SetVoice", Expression: voice, Message: voice};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function sendMouth(emotion)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Mouth", Expression: emotion};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function closeBrowser()
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "KillBrowser"};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

function RequestRecording(recordingType, command)
{
    if (websocket.readyState == WebSocket.OPEN)
    {
        var obj = {Command: "Record", Message: recordingType + ", " + command};
       
        websocket.send(JSON.stringify(obj));
    }
    else
    {
        emit("Socket not open, state: " + websocket.readyState);
    }
}

  window.addEventListener("load", init, false);
