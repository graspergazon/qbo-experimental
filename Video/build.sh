#!/bin/bash
#sudo apt update
#sudo apt install git -y


CHECKGIT=`which git`
if [ -z "$CHECKGIT" ]; then
    echo "Git is not installed, trying to install..."
    sudo apt update
    sudo apt install git
else   
    echo "Git is installed"
fi

CHECKDOT=`which dotnet`
if [ -z "$CHECKDOT" ]; then    
    echo "dotnet is not installed"
else    
    echo "found dotnet"

fi

echo "Checking directories.."

BASEDIR=/home/pi/vv
if [ -d "$BASEDIR" ]; then
    echo "basedir exists"
else
    echo "basedir does not exist"  
    mkdir -p $BASEDIR
fi


GITDIR="$BASEDIR/firstpilot"
if [ -d "$GITDIR" ]; then
    echo "git dir exists"
else
    echo "git dir does not exist, cloning "
    cd $BASEDIR
    git clone  https://gitlab.com/vicarvision/grants/aid2bewell/firstpilot.git       
fi

echo "Building dotnet projects..."

dotnet build $GITDIR/Video/VideoRecordMqtt/VideoRecordMqtt.csproj  -c Release

echo "Copying files to build folder"

mkdir -p $BASEDIR/Video/bin

cp -a $GITDIR/Video/VideoRecordMqtt/bin/Release/net5.0/. $BASEDIR/Video/bin

SCRIPTS="$BASEDIR/scripts"

mkdir -p $SCRIPTS
cp -a $GITDIR/Scripts/. $SCRIPTS

ALLSCRIPS="$SCRIPTS/*"
for f in $ALLSCRIPS
do
    echo "Processing file $f"
    chmod +x $f
done

echo "Done!"

echo "Done!"
