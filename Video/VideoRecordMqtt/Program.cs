﻿using System;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using System.Diagnostics;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Runtime.InteropServices;
using System.IO;
using Serilog;
using System.Threading.Tasks;
using System.Threading;

namespace VideoRecordMqtt
{
    class Program
    {

        private const string TOPIC_IN_RECORD = "qbo/video/record";
        private const string TOPIC_IN_SNAPSHOT = "qbo/video/snapshot";
        private const string TOPIC_OUT_STATUS = "qbo/video/status";
        private const string TOPIC_OUT_IMAGE = "qbo/video/image";
        private const string TOPIC_IN_SHUTDOWN = "qbo/video/shutdown";

        private const string MQTT_BROKER_ADDRESS = "87.214.54.242";

        private static bool _isRecoring;
        private static Process _process;
        private static MqttClient _client;
        private static bool _isRunning;


        static void Main(string[] args)
        {

            var exitEvent = new ManualResetEvent(initialState: false);
            AppDomain.CurrentDomain.ProcessExit += (o, e) => exitEvent.Set();

            InitLogger();

            _client = new MqttClient(MQTT_BROKER_ADDRESS);
            _client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            _client.ConnectionClosed += Client_ConnectionClosed;

            string clientId = Guid.NewGuid().ToString();
            _client.Connect(clientId);

            if (_client.IsConnected)
            {
                Console.WriteLine("Connected with mqtt broker");
                Log.Information("Connected with mqtt broker");

                SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Video Client Connected");

            }

            // subscribe to the topic "/home/temperature" with QoS 2
            _client.Subscribe(new string[] { TOPIC_IN_RECORD, TOPIC_IN_SNAPSHOT, TOPIC_IN_SHUTDOWN }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });


            //System.Threading.Thread pingThread = new System.Threading.Thread(PingPong);
            //pingThread.Start();

            exitEvent.WaitOne();
            SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Shutting down Video Client");
            //Console.ReadLine();

            if (_client != null)
            {
                _client.Unsubscribe(new string[] { TOPIC_IN_RECORD });
                _client.MqttMsgPublishReceived -= Client_MqttMsgPublishReceived;
                _client.ConnectionClosed -= Client_ConnectionClosed;
                _client.Disconnect();
                _client = null;
            }

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.Kill();
                        _process.Close();
                        _process = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());

                byte[] data = System.Text.Encoding.UTF8.GetBytes("Error");
                _client.Publish(TOPIC_OUT_STATUS, data);
            }

            Environment.Exit(0);


        }

        private static void PingPong() 
        {
            _isRunning = true;

            int count = 0;
            while (_isRunning) 
            {
                System.Threading.Thread.Sleep(5000); 

                if(count%2==0)
                    SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Video Client Ping");
                else
                    SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Video Client Ping");


                count++;

            }
        }

        private static void SendStatus(string message) 
        {
            try
            {
                if (_client != null && _client.IsConnected) 
                {
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
                    _client.Publish(TOPIC_OUT_STATUS, data);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log.Error(ex, "Error sending status over mqtt");
               
            }
        }

        private static void Client_ConnectionClosed(object sender, EventArgs e)
        {
            Log.Information("Closed connection with mqtt broker");

            Console.WriteLine("Closed connection with mqtt broker");
        }

        private static async void Client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string topic = e.Topic;


            string message = System.Text.Encoding.UTF8.GetString(e.Message);

            Console.WriteLine($"Receiving message with topic {topic} and text {message}");
            Log.Information($"Received a message with topic {topic} and text {message}");


            if (topic.ToLower().Contains(TOPIC_IN_RECORD))
            {
                if (message.ToLower().Contains("start"))
                {

                    StartRecording();
                }
                else if (message.ToLower().Contains("stop"))
                {
                    StopRecording();
                }

                else if (message.ToLower().Contains("status"))
                {
                    if (_process != null)
                    {

                        if (_process.HasExited)
                        {
                            Console.WriteLine("Process has exited");
                            SendStatus("Video recording process has exited");

                        }
                        else
                        {
                            Console.WriteLine($"Process {_process.Id} is running");
                            SendStatus("Recording video");

                        }
                    }
                    else
                        Console.WriteLine("Process is null");

                }
            }
            else if (topic.ToLower().Contains(TOPIC_IN_SNAPSHOT))
            {

                if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                {
                    ReadImageAndSendData();


                }
                else
                {
                    TakeSnapShotAndSendData();
                }


            }
            else if (topic.ToLower().Contains(TOPIC_IN_SHUTDOWN)) 
            {
                await ShutDownThisMachine();
            }
        }

        private static async Task ShutDownThisMachine()
        {
            await System.Threading.Tasks.Task.Run(new Action(() =>
            {

                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.UseShellExecute = false;
                    startInfo.FileName = "/bin/bash"; // Specify exe name.
                    startInfo.Arguments = $"/home/pi/vv/scripts/shutdown.sh";

                    SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Shutting down external Pi");


                    using (Process p = Process.Start(startInfo))
                    {
                        p.WaitForExit(5000);

                        if (p.HasExited)
                        {
                            Log.Information($"Shutting down");

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error shutting down");


                }
            }));
        }

        private static void ReadImageAndSendData() 
        {
            string filePath = @"E:\Code\QBO\image.jpg";

            if (_client == null)
                return;

            if (!_client.IsConnected)
            {
                return;
            }


            try
            {
                if (File.Exists(filePath))
                {
                    byte[] buffer = new byte[2 * 1024 * 1024];//2MB
                    int bytesRead = 0;
                    using (FileStream stream = new FileStream(filePath, FileMode.Open))
                    {
                        bytesRead = stream.Read(buffer, 0, buffer.Length);
                    }


                    if (bytesRead > 0)
                    {
                        if (_client != null && _client.IsConnected)
                        {
                            byte[] data = new byte[bytesRead];
                            Buffer.BlockCopy(buffer, 0, data, 0, bytesRead);
                            _client.Publish(TOPIC_OUT_IMAGE, data);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Log.Error(ex, "Error in creating or sending snapshot");
            }
        }
        

        private static async Task TakeSnapShotAndSendData() 
        {
            string filePath = @"/home/pi/Pictures/image.jpg";

            await System.Threading.Tasks.Task.Run(new Action(() =>
            {
                SendStatus($"{DateTime.Now.ToString("HH:mm:ss yy-MM-dd")} Taking snapshot");

                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.UseShellExecute = false;
                    startInfo.FileName = @"fswebcam"; // Specify exe name.
                    startInfo.Arguments = $"-r 640x480 --no-banner {filePath}";

                    using (Process p = Process.Start(startInfo))
                    {
                        p.WaitForExit(5000);

                        if (p.HasExited)
                        {
                            Log.Information($"Created a jpg file using fswebcam");

                        }
                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error creating jpg file");


                }
            }));

            if (_client == null)
                return;

            if (!_client.IsConnected) 
            {
                return;
            }


            try 
            {
                if (File.Exists(filePath)) 
                {
                    byte[] buffer = new byte[2 * 1024 * 1024];//2MB
                    int bytesRead = 0;
                    using (FileStream stream = new FileStream(filePath, FileMode.Open)) 
                    {
                        bytesRead = stream.Read(buffer, 0, buffer.Length);
                    }


                    if (bytesRead > 0) 
                    {
                        if (_client != null && _client.IsConnected) 
                        {
                            byte[] data = new byte[bytesRead];
                            Buffer.BlockCopy(buffer, 0, data, 0, bytesRead);
                            _client.Publish(TOPIC_OUT_IMAGE, data);
                        }
                    }
                }
            }
            catch (Exception ex) 
            {
                Log.Error(ex, "Error in creating or sending snapshot");
            }
        }
        private static void StartRecording()
        {
            if (_isRecoring)
                return;

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.StandardInput.WriteLine("q");
                        _process.StandardInput.Flush();
                        _process.WaitForExit(2000);
                        _process.Close();
                        _process = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine("Starting video recording");

            string fileName = "vid_" + DateTime.Now.ToString("yyyy_MM_dd__HH_mm_ss");
            string bitrate = "512k";

            ProcessStartInfo start = new ProcessStartInfo();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                start.FileName = @"notepad.exe"; // Specify exe name.


            }
            else
            {
                start.FileName = @"ffmpeg"; // Specify exe name.
                start.Arguments = $"-input_format yuyv422 -f video4linux2 -s 640x480 -r 15 -i /dev/video0 -c:v h264_omx -r 15 -b:v {bitrate} /home/pi/vv/{fileName}.mp4";
            }
            start.UseShellExecute = false;
            start.RedirectStandardOutput = false;
            start.RedirectStandardError = false;
            start.RedirectStandardInput = true;


            Console.WriteLine("Starting recording");
            Log.Information($"Starting recording process with filename {fileName}.wav");
            try
            {
                _process = Process.Start(start);
                _isRecoring = true;
                SendStatus("Recoding video");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log.Error(ex, "Error starting recording process");

            }
        }


        private static void InitLogger()
        {
            string logDir = "Logs";


            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                logDir = "/home/pi/logs";
            }

            try
            {
                if (!Directory.Exists(logDir))
                {
                    Directory.CreateDirectory(logDir);
                }
            }
            catch (Exception ex)
            { }

            Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.File(Path.Combine(logDir, "Log.txt"), rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information)
                 .WriteTo.File(Path.Combine(logDir, "ErrorLog.txt"), rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error)
                 .CreateLogger();
        }
        private static void StopRecording()
        {
            if (!_isRecoring)
                return;

            Console.WriteLine("Stopping audio recording");

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.StandardInput.WriteLine("q");
                        _process.StandardInput.Flush();
                        _process.WaitForExit(2);
                        _process.Close();

                        SendStatus("Video recording stopped");
                        _process = null;
                    }
                }

                _isRecoring = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log.Error(ex, "Error stopping recording process");
            }

        }
    }
}
