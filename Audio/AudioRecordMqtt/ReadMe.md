﻿This applications connects to a MQTT broker and listens to topic 'qbo/audio'.  
When it receives a message with the text 'start' it starts a recording by calling an external process (arecord).  
When it receives a 'stop' message it kills the recording process.