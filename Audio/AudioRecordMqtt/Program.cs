﻿using System;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using System.Diagnostics;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Runtime.InteropServices;
using System.IO;
using Serilog;
using System.Text.Json;
using IBM.Watson.TextToSpeech.v1;
using IBM.Cloud.SDK.Core.Authentication.Iam;
using System.Threading;
using System.Collections.Concurrent;

namespace AudioRecordMqtt
{
    class Program
    {

        private const string TOPIC_IN_RECORD = "qbo/audio/record";
        private const string TOPIC_IN_PLAY = "qbo/audio/play";
        private const string TOPIC_OUT = "qbo/audio/status";
        private const string MQTT_BROKER_ADDRESS = "87.214.54.242";

        private const string WATSON_API_KEY = "x1F9JrWvm0S5I4hejtY3ZUPSAv_C7RTDI2Q7l2aEjwaB";


        private static bool _isRecoring;
        private static Process _process;
        private static MqttClient _client;
        private static TextToSpeechService _textToSpeech;
        private static ConcurrentQueue<AudioMessage> _audioQueue;
        private static bool _isProcessingAudio = false;

        public static string voiceGender = "male";


        static void Main(string[] args)
        {
            var exitEvent = new ManualResetEvent(initialState: false);
            AppDomain.CurrentDomain.ProcessExit += (o, e) => exitEvent.Set();

            _audioQueue = new ConcurrentQueue<AudioMessage>();

            InitLogger();

            _client = new MqttClient(MQTT_BROKER_ADDRESS);
            _client.MqttMsgPublishReceived += Client_MqttMsgPublishReceived;
            _client.ConnectionClosed += Client_ConnectionClosed;

            string clientId = Guid.NewGuid().ToString();
            _client.Connect(clientId);

            if (_client.IsConnected) 
            {
                Console.WriteLine("Connected with mqtt broker");
                Log.Information("Connected with mqtt broker");

            }

            // subscribe to the topic "/home/temperature" with QoS 2
            _client.Subscribe(new string[] { TOPIC_IN_RECORD, TOPIC_IN_PLAY }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE, MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE });


            InitWatsonTTS();

            AudioFileGenerator generator = new AudioFileGenerator(_textToSpeech);
            //generator.GenerateGenderChoice("male");
            //generator.GenerateGenderChoice("female");
            //generator.GenerateInstructions("male");
            //generator.GenerateInstructions("female");

            exitEvent.WaitOne();

            if (_client != null) 
            {
                _client.Unsubscribe(new string[] { TOPIC_IN_RECORD });
                _client.MqttMsgPublishReceived -= Client_MqttMsgPublishReceived;
                _client.ConnectionClosed -= Client_ConnectionClosed;
                _client.Disconnect();
                _client = null;
            }

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.Kill();
                        _process.Close();
                        _process = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Environment.Exit(0);
           
           
        }

        private static void InitWatsonTTS() 
        {
            IamAuthenticator authenticator = new IamAuthenticator(apikey: WATSON_API_KEY);

            _textToSpeech = new TextToSpeechService(authenticator);
            _textToSpeech.SetServiceUrl("https://api.eu-de.text-to-speech.watson.cloud.ibm.com");
        }

        private static void SendStatus(string message)
        {
            try
            {
                if (_client != null && _client.IsConnected)
                {
                    byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
                    _client.Publish(TOPIC_OUT, data);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log.Error(ex, "Error sending status over mqtt");

            }
        }

        private static void Client_ConnectionClosed(object sender, EventArgs e)
        {
            Log.Information("Closed connection with mqtt broker");

            Console.WriteLine("Closed connection with mqtt broker");
        }

        private static async void Client_MqttMsgPublishReceived(object sender, uPLibrary.Networking.M2Mqtt.Messages.MqttMsgPublishEventArgs e)
        {
            string topic = e.Topic;


            string message = System.Text.Encoding.UTF8.GetString(e.Message);

            Console.WriteLine($"Receiving message with topic {topic} and text {message}");
            Log.Information($"Received a message with topic {topic} and text {message}");


            if (topic.ToLower().Contains(TOPIC_IN_RECORD))
            {
                if (message.ToLower().Contains("start"))
                {

                    StartRecording();
                }
                else if (message.ToLower().Contains("stop"))
                {
                    StopRecording();
                }
                else if (message.ToLower().Contains("status"))
                {
                    if (_process != null)
                    {
                        if (_process.HasExited)
                        {
                            Console.WriteLine("Process has exited");
                            SendStatus("Audio recording process has exited");
                        }
                        else
                        {
                            Console.WriteLine($"Process {_process.Id} is running");
                            SendStatus("Recording audio");


                        }
                    }
                    else
                    {
                        Console.WriteLine("process is null");
                    }
                }
            }
            else if (topic.ToLower().Contains(TOPIC_IN_PLAY)) 
            {
                string txt = message;

                try
                {
                    AudioMessage? audioCommand = JsonSerializer.Deserialize<AudioMessage>(txt);

                    //if (audioCommand.MessageType.ToLower().Contains("tts"))
                    //{
                    //    await DoTextToSpeech(audioCommand);
                    //}
                    //else if (audioCommand.MessageType.ToLower().Contains("playfile")) 
                    //{
                    //    await PlayAudioFile(audioCommand);

                    //}

                    if (_isProcessingAudio) 
                    {
                        _audioQueue.Enqueue(audioCommand);
                        return;
                    }

                    await ProcessAudioMessage(audioCommand);
                    _isProcessingAudio = false;
                }
                catch (Exception ex) 
                {
                    Log.Logger.Error(ex, "Error playing audio");
                }

                
            }

            
        }

        private static async System.Threading.Tasks.Task ProcessAudioMessage(AudioMessage message) 
        {
            if (message.MessageType.ToLower().Contains("tts"))
            {
                await DoTextToSpeech(message);
            }
            else if (message.MessageType.ToLower().Contains("playfile"))
            {
                await PlayAudioFile(message);

            }

            AudioMessage nextMessage = null;

            if (_audioQueue.TryDequeue(out nextMessage)) 
            {
                await ProcessAudioMessage(nextMessage);
            }
        }


        private static async System.Threading.Tasks.Task DoTextToSpeech(AudioMessage message) 
        {
            if (message==null || string.IsNullOrEmpty(message.Text))
                return;


            _isProcessingAudio = true;

            //string liamVoice = "nl-NL_LiamVoice";
            //string emmaVoice = "nl-NL_EmmaVoice";
            string voice = "nl-NL_EmmaVoice";

            if (!string.IsNullOrEmpty(message.Voice))
                voice = message.Voice;

            string filename = "/home/pi/Music/watson.wav";


            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                filename = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments),"Audio", "watson.wav");
            }
            


            await System.Threading.Tasks.Task.Run(new Action(() =>
            {
                try
                {
                    

                    var spreechResult = _textToSpeech.Synthesize(text: message.Text, accept: "audio/wav", voice: voice);

                    using (FileStream fs = File.Create(filename))
                    {
                        spreechResult.Result.WriteTo(fs);
                        fs.Close();
                        spreechResult.Result.Close();
                    }

                    
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error creating wav file");


                }
            }));

            await System.Threading.Tasks.Task.Run(new Action(() =>
            {
                try
                {
                    ProcessStartInfo startInfo = new ProcessStartInfo();
                    startInfo.UseShellExecute = false;
                    startInfo.FileName = @"aplay"; // Specify exe name.

                    if (!string.IsNullOrEmpty(message.OutputType) && message.OutputType.ToLower().Contains("external")) 
                    {
                        startInfo.Arguments = $"-D sysdefault:CARD=ALSA {filename}";
                    }
                    else
                        startInfo.Arguments = $"-D convertQBO {filename}";


                    if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                    {
                        startInfo.FileName = @"C:\Users\kasper\Documents\Audio\mplayer-x86_64\mplayer.exe"; // Specify exe name.
                        startInfo.Arguments = $"{filename}";
                    }


                    Log.Information($"playing audio file");

                    using (Process p = Process.Start(startInfo))
                    {
                        p.WaitForExit(10000);
                        Log.Information($"Ended playing audio file");

                    }
                }
                catch (Exception ex)
                {
                    Log.Error(ex, "Error playing wav file");

                }
            }));

            _isProcessingAudio = false;
        }

        private static async System.Threading.Tasks.Task PlayAudioFile(AudioMessage message)
        {
            if (message == null)
                return;

            if (string.IsNullOrEmpty(message.Filename))
                return;

            if (!string.IsNullOrEmpty(message.Voice)) 
            {
                voiceGender = message.Voice;
            }


            if (message.Filename.ToLower().Contains(".wav"))
            {
                
            }
            else 
            {
                message.Filename = $"{message.Filename}{voiceGender}.wav";
            }
            _isProcessingAudio = true;
            string filePath = Path.Combine("/home/pi/Music", message.Filename);

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                filePath = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments), "Audio", message.Filename);
            }

            if (!File.Exists(filePath))
            {
                Log.Information($"File {filePath} does not exist");

                return;
            }

            if (message.Delay > 0) 
            {
                await System.Threading.Tasks.Task.Delay(message.Delay);
            }

            if (!string.IsNullOrEmpty(message.OutputType) && message.OutputType.ToLower().Contains("external"))
            {
                await System.Threading.Tasks.Task.Run(new Action(() =>
                {
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.UseShellExecute = false;
                        startInfo.FileName = @"aplay"; // Specify exe name.
                        startInfo.Arguments = $"-D sysdefault:CARD=ALSA {filePath}";

                        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                        {
                            startInfo.FileName = @"C:\Users\kasper\Documents\Audio\mplayer-x86_64\mplayer.exe"; // Specify exe name.
                            startInfo.Arguments = $"{filePath}";
                        }

                        Log.Information($"playing audio file");

                        using (Process p = Process.Start(startInfo))
                        {
                            p.WaitForExit(10000);
                            Log.Information($"Ended playing audio file");

                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error playing wav file");

                    }
                }));
            }
            else
            {

                await System.Threading.Tasks.Task.Run(new Action(() =>
                {
                    try
                    {
                        ProcessStartInfo startInfo = new ProcessStartInfo();
                        startInfo.UseShellExecute = false;
                        startInfo.FileName = @"aplay"; // Specify exe name.
                    startInfo.Arguments = $"-D convertQBO {filePath}";

                        if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
                        {
                            startInfo.FileName = @"C:\Users\kasper\Documents\Audio\mplayer-x86_64\mplayer.exe"; // Specify exe name.
                            startInfo.Arguments = $"{filePath}";
                        }

                        Log.Information($"playing audio file");

                        using (Process p = Process.Start(startInfo))
                        {
                            p.WaitForExit(10000);
                            Log.Information($"Ended playing audio file");

                        }
                    }
                    catch (Exception ex)
                    {
                        Log.Error(ex, "Error playing wav file");

                    }
                }));
            }

            _isProcessingAudio = false;
        }
        private static void StartRecording() 
        {
            if (_isRecoring)
                return;

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.Kill();
                        _process.Close();
                        _process = null;
                    }
                }
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine("Starting audio recording");

            string fileName = "audio_" + DateTime.Now.ToString("yyyy_MM_dd__HH_mm_ss");

            ProcessStartInfo start = new ProcessStartInfo();

            if (RuntimeInformation.IsOSPlatform(OSPlatform.Windows))
            {
                start.FileName = @"notepad.exe"; // Specify exe name.

                
            }
            else 
            {
                start.FileName = @"arecord"; // Specify exe name.
                start.Arguments = $"--device=hw:1,0 --format S16_LE --rate 16000 -c 2 /home/pi/vv/{fileName}.wav";
            }
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;
          


            Console.WriteLine("Starting recording");
            Log.Information($"Starting recording process with filename {fileName}.wav");
            try
            {
                _process = Process.Start(start);
                _isRecoring = true;
                SendStatus("Recoding audio");
            }
            catch (Exception ex) 
            {
                Console.WriteLine(ex.ToString());
                Log.Error(ex, "Error starting recording process");

            }
        }


        private static void InitLogger() 
        {
            string logDir = "Logs";


            if (RuntimeInformation.IsOSPlatform(OSPlatform.Linux))
            {
                logDir = "/home/pi/logs";
            }

            try
            {
                if (!Directory.Exists(logDir))
                {
                    Directory.CreateDirectory(logDir);
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine("Could not create dir: " + ex.ToString());
            }

            Log.Logger = new LoggerConfiguration().MinimumLevel.Debug().WriteTo.File(Path.Combine(logDir, "Log.txt"), rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Information)
                 .WriteTo.File(Path.Combine(logDir, "ErrorLog.txt"), rollingInterval: RollingInterval.Day, restrictedToMinimumLevel: Serilog.Events.LogEventLevel.Error)                 
                 .CreateLogger();
        }
        private static void StopRecording()
        {
            if (!_isRecoring)
                return;

            Console.WriteLine("Stopping audio recording");

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.Kill();
                        _process.Close();
                        SendStatus("Audio recording stopped");

                        _process = null;
                    }
                }

                _isRecoring = false;
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
                Log.Error(ex, "Error stopping recording process");
            }

        }
    }
}
