﻿using IBM.Watson.TextToSpeech.v1;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AudioRecordMqtt
{
    public class AudioFileGenerator
    {
        private TextToSpeechService _textToSpeech;


        private string[] _surveyQuestions;
        private string[] _surveyAns1;
        private string[] _surveyAns2;

        private string[] _otherQuestions;
        private string[] _otheryAns1;
        private string[] _otherAns2;

        private string[] _instructions;
        public AudioFileGenerator(TextToSpeechService textToSpeech)
        {
            _textToSpeech = textToSpeech;
            InitSurvey();
            InitOther();
            InitInstructions();
            
        }


        public void GenerateSurvey(string gender) 
        {
            string voice = "nl-NL_LiamVoice";
            if (gender.Contains("female"))
            {
                voice = "nl-NL_EmmaVoice";
            }

            for (int i = 0; i < _surveyQuestions.Length; i++)
            {
                string filename = $"Survey_q_{i}_{gender}.wav";
                Generate(_surveyQuestions[i], voice, filename);
                filename = $"Survey_ans1_{i}_{gender}.wav";
                Generate(_surveyAns1[i], voice, filename);
                filename = $"Survey_ans2_{i}_{gender}.wav";
                Generate(_surveyAns2[i], voice, filename);
            }
        }

        public void GenerateInstructions(string gender)
        {
            string voice = "nl-NL_LiamVoice";
            if (gender.Contains("female"))
            {
                voice = "nl-NL_EmmaVoice";
            }

            for (int i = 0; i < _instructions.Length; i++)
            {
                string filename = $"Instruction_q_{i}_{gender}.wav";
                Generate(_instructions[i], voice, filename);
                
            }
        }

        public void GenerateGenderChoice(string gender)
        {
            string voice = "nl-NL_LiamVoice";
            if (gender.Contains("female"))
            {
                voice = "nl-NL_EmmaVoice";
            }

            
            string filename = $"VoiceSelect_q_{gender}.wav";
            Generate("U kunt nu een stem voor mij kiezen.", voice, filename);

            filename = $"VoiceSelect_ans_{gender}.wav";
            Generate("U heeft nu deze stem voor mij gekozen.", voice, filename);

            filename = $"Expression_q_{gender}.wav";
            Generate("Kunt u deze gezichtsuitdrukking na doen?", voice, filename);           
            
        }

        public void GenerateQuestions(string gender)
        {
            string voice = "nl-NL_LiamVoice";
            if (gender.Contains("female"))
            {
                voice = "nl-NL_EmmaVoice";
            }

            for (int i = 0; i < _otherQuestions.Length; i++)
            {
                string filename = $"Other_q_{i}_{gender}.wav";
                Generate(_otheryAns1[i], voice, filename);
                filename = $"Other_ans1_{i}_{gender}.wav";
                Generate(_surveyAns1[i], voice, filename);
                filename = $"Other_ans2_{i}_{gender}.wav";
                Generate(_otherAns2[i], voice, filename);
            }
        }

        public void Generate(string txt, string voice, string filename) 
        {
            var spreechResult = _textToSpeech.Synthesize(text: txt, accept: "audio/wav", voice: voice);

            using (FileStream fs = File.Create(filename))
            {
                spreechResult.Result.WriteTo(fs);
                fs.Close();
                spreechResult.Result.Close();
            }
        }
        private void InitSurvey()
        {
            _surveyQuestions = new string[]
            {
                "Heeft u een smartphone?",
                "Praat je graag met mij?",
                "Zou u het gezellig vinden als ik in uw woonkamer zou staan?",
                "Zie ik er goed uit vandaag?",
                "Luistert u graag naar muziek?",
                "Wilt u een mop horen?"
            };

            _surveyAns1 = new string[]
            {
                "Dat is handig!",
                "Dat vind ik leuk!",
                "Ik ook!",
                "Dankjewel!",
                "Ik ook!",
                "Wat is het lievelings muziekgenre van een robot? Heavy metal."
            };

            _surveyAns2 = new string[]
            {
                "Lekker rustig.",
                "Dat vind ik jammer.",
                "Dat vind ik jammer.",
                "Ik heb mijn best gedaan.",
                "Ik ook niet.",
                "Dat is goed!"
            };

        }

        private void InitInstructions()
        {
            _instructions = new string[]
            {
                "Ik ga een aantal vragen stellen.",
                "Geef antwoord door ja of nee te zeggen.",
                "Geef antwoord via de knoppen op de tablet.",
                "Raak voor ja de linkerkant van mijn hoofd aan en voor nee de rechterkant."
            };
        }

        private void InitOther()
        {
            _otherQuestions = new string[]
            {
                "Is het mooi weer?",
                "Ben je vandaag buiten geweest?",
                "Is het café gezellig?",
                "Heb je gister iets leuks gedaan?",
                "Beweeg je graag?",
                "Heb je vandaag al geluncht?"
            };

            _otheryAns1 = new string[]
            {
                "Dat is fijn!",
                "Dat is lekker!",
                "Dat vind ik ook!",
                "Dat is leuk!",
                "Dat is gezond!",
                "Dat is lekker!"
            };

            _otherAns2 = new string[]
            {
                "Dat is jammer!",
                "Misschien straks nog even.",
                "Dat is jammer!",
                "Een andere keer.",
                "Ik beweeg ook niet veel.",
                "Eet smakelijk alvast!"
            };

        }
    }
}