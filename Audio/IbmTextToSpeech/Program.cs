﻿using System;
using System.IO;
using System.Text;
using System.Diagnostics;
using IBM.Cloud.SDK.Core.Authentication.Iam;
using IBM.Watson.TextToSpeech.v1;

namespace IbmTextToSpeech
{
    class Program
    {
        private static TextToSpeechService _textToSpeech;
        static void Main(string[] args)
        {
            

            Console.WriteLine("Starting recording");
            string apikey ="x1F9JrWvm0S5I4hejtY3ZUPSAv_C7RTDI2Q7l2aEjwaB";

            IamAuthenticator authenticator = new IamAuthenticator(apikey: apikey  );

            _textToSpeech = new TextToSpeechService(authenticator);
            _textToSpeech.SetServiceUrl("https://api.eu-de.text-to-speech.watson.cloud.ibm.com");

            //var result = textToSpeech.ListVoices();

            //foreach (var item in result.Result._Voices)
            //{
            //    Console.WriteLine($"{item.Language}, {item.Name}, {item.Gender}, {item.Description}\n\n");
            //}
            

            string[] questions = new string[]
            {
                "Gaat het goed met je?",
                "Kun je mij goed verstaan?",
                "Is deze text leesbaar?",
                "Is dit beter?"

            };

            string liamVoice = "nl-NL_LiamVoice";
            string emmaVoice = "nl-NL_EmmaVoice";

            for (int i = 0; i < questions.Length; i++)
            {
                GenerateWavFile(questions[i], $"q{i}_male.wav", liamVoice);
                GenerateWavFile(questions[i], $"q{i}_female.wav", emmaVoice);
            }
            //nl-NL_EmmaVoice

        }

        private static void GenerateWavFile(string speechText, string filename, string voice) 
        {
           

            var spreechResult = _textToSpeech.Synthesize(text: speechText, accept: "audio/wav", voice: voice);

            using (FileStream fs = File.Create(filename))
            {
                spreechResult.Result.WriteTo(fs);
                fs.Close();
                spreechResult.Result.Close();
            }
        }
    }
}