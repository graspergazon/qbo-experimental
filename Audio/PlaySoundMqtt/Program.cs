﻿using System;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using System.Diagnostics;
using uPLibrary.Networking.M2Mqtt.Messages;
using System.Runtime.InteropServices;
using System.IO;
using Serilog;

namespace PlaySoundMqtt
{
    class Program
    {

        private const string TOPIC_IN = "qbo/video/record";
        private const string TOPIC_OUT = "qbo/video/status";
        private const string MQTT_BROKER_ADDRESS = "87.214.54.242";

        private static bool _isRecoring;
        private static Process _process;
        private static MqttClient _client;
        static void Main(string[] args)
        {

            _client = new MqttClient(MQTT_BROKER_ADDRESS);
            _client.MqttMsgPublishReceived += _client_MqttMsgPublishReceived;
            _client.ConnectionClosed += _client_ConnectionClosed;

            string clientId = Guid.NewGuid().ToString();
            _client.Connect(clientId);
            if (_client.IsConnected)
            {
                Console.WriteLine("Connected with mqtt broker");
                

            }


            Console.WriteLine("Enter txt en press enter. To exit type q + enter");

            while (true) 
            {
                string txt = Console.ReadLine();

                if (txt != null)
                {
                    if (txt.Length == 1 && txt.ToLower().Contains("q"))
                    {
                        break;
                    }

                    SendText(txt);

                }
                else 
                {

                }
            }

            if (_client != null)
            {
                //_client.Unsubscribe(new string[] { TOPIC_IN_RECORD });
                //_client.MqttMsgPublishReceived -= Client_MqttMsgPublishReceived;
                //_client.ConnectionClosed -= Client_ConnectionClosed;
                _client.Disconnect();
                _client = null;
            }

            try
            {
                if (_process != null)
                {
                    if (!_process.HasExited)
                    {
                        _process.Kill();
                        _process.Close();
                        _process = null;
                    }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Environment.Exit(0);


        }
        private static void SendText(string message) 
        {
            if (_client == null)
                return;

            if (!_client.IsConnected)
                return;
            byte[] data = System.Text.Encoding.UTF8.GetBytes(message);
            _client.Publish("qbo/audio/play", data);
        }
        private static void _client_ConnectionClosed(object sender, EventArgs e)
        {
            
        }

        private static void _client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e)
        {
           
        }
    }
}
